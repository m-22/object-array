<?php

namespace M22\ObjectArray\Tools;

use M22\ObjectArray\Tools\Attributes\Generator\FixedDefault;
use M22\ObjectArray\Tools\Attributes\Generator\FixedTypes;
use Nette\PhpGenerator\Literal;

final class Param {

  public readonly string $name;

  public readonly int $position;

  private Types|false $types;

  public function __construct(
    public readonly \ReflectionParameter $reflectionParameter,
    public readonly Func $func,
  ) {
    $this->name = $this->reflectionParameter->name;
    $this->position = $this->reflectionParameter->getPosition();
  }

  /**
   * Indicates whether an optional parameter has no default value.
   *
   * Such parameter may exist in a function with multiple signatures (e.g.,
   * array_keys), or in other cases (e.g., array_walk and array_walk_recursive).
   */
  public function isOptionalWithoutDefault(): bool {
    $ref = $this->reflectionParameter;
    return $this->isOptionalNotVariadic() && !$ref->isDefaultValueAvailable();
  }

  /**
   * Indicates whether a parameter can't be imitated in a user-land.
   *
   * This is a parameter which can be passed both by value and by reference
   * (e.g., array_multisort).
   */
  public function isInimitable(): bool {
    $ref = $this->reflectionParameter;
    return $this->isPassedByReference() && $ref->canBePassedByValue();
  }

  public function isOptionalNotVariadic(): bool {
    return $this->reflectionParameter->isOptional() && !$this->isVariadic();
  }

  public function isVariadic(): bool {
    return $this->reflectionParameter->isVariadic();
  }

  public function isPassedByReference(): bool {
    return $this->reflectionParameter->isPassedByReference();
  }

  public function getTypes(): ?Types {
    if (!isset($this->types)) {
      $raw_types = (!$this->getAttributes(FixedTypes::class) ? $this->reflectionParameter->getType() : NULL)
        ?? $this->func->getMethodReflectionParameter($this->name)?->getType()
        ?? $this->getTypesFromMethodDocComment();

      $this->types = $raw_types
        ? Types::fromStringable($raw_types)->addObjectArrayIfPossible()
        : FALSE;
    }

    return $this->types ?: NULL;
  }

  public function getDefaultValueLiteral(): ?Literal {
    $ref = $this->getAttributes(FixedDefault::class)
      ? $this->func->getMethodReflectionParameter($this->name)
      : $this->reflectionParameter;

    if ($ref->isDefaultValueAvailable()) {
      return $ref->isDefaultValueConstant()
        ? new Literal('\\' . ltrim($ref->getDefaultValueConstantName(), '\\'))
        : new Literal('?', [$ref->getDefaultValue()]);
    }

    return NULL;
  }

  public function mayBeSkipped(): bool {
    if ($this->isFixedArg()) {
      if ($this->isVariadic()) {
        if ($this->getFixedArgValue() !== []) {
          throw new \Exception('Variadic parameter cannot have fixed value other than an empty array.');
        }

        return TRUE;
      }

      $ref = $this->reflectionParameter;
      return $ref->isDefaultValueAvailable()
        && $ref->getDefaultValue() === $this->getFixedArgValue();
    }

    return FALSE;
  }

  public function isFixedArg(): bool {
    return array_key_exists($this->name, $this->func->getFixedArgs());
  }

  public function getFixedArgValue(): mixed {
    return $this->func->getFixedArgs()[$this->name];
  }

  public function getTypesFromMethodDocComment(): ?string {
    if ($doc = $this->func->reflectionMethod?->getDocComment()) {
      if (preg_match('/@param (?<types>\S+) (\.\.\.)?\$' . $this->name . '\b/', $doc, $matches)) {
        return $matches['types'];
      }
    }

    return NULL;
  }

  public function document(): string {
    $doc = '@param ';

    if ($types = $this->getTypes()) {
      $doc .= $types->toString() . ' ';
    }

    if ($this->isVariadic()) {
      $doc .= '...';
    }

    $doc .= '$' . $this->name;

    if ($this->isOptionalNotVariadic()) {
      if ($default = $this->getDefaultValueLiteral()) {
        $doc .= ' = ' . $default;
      }

      $doc .= ' (optional)';
    }

    return $doc;
  }

  /**
   * @return \ReflectionAttribute[]
   */
  public function getAttributes(?string $name = NULL): array {
    return $this->func->getMethodReflectionParameter($this->name)?->getAttributes($name)
      ?? $this->reflectionParameter->getAttributes($name);
  }

  public function canBeDefaultArrayParam(): int {
    $types = $this->getTypes();

    if ($types?->isArrayAllowed() && (!$this->isPassedByReference() || !$types->isMixed())) {
      if (!$types->isMixed() && !$this->isVariadic()) {
        if ($this->name === 'subject') {
          return 3;
        }

        return 2;
      }

      return 1;
    }

    return 0;
  }

}
