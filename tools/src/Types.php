<?php

namespace M22\ObjectArray\Tools;

use M22\ObjectArray;

final class Types extends ObjectArray implements \Stringable {

  /**
   * List of types and whether they're "useful" class-types or not.
   *
   * @var bool[]
   */
  private static array $usefulClassTypes = [
    'resource' => FALSE,
  ];

  public static function fromStringable(string|\Stringable $types): self {
    if ($types instanceof \ReflectionType) {
      foreach ($types instanceof \ReflectionUnionType ? $types->getTypes() : [$types] as $type) {
        if ($type instanceof \ReflectionNamedType) {
          // The getName() returns pure type name, w/o '?' prefix for nullables.
          // Built-in type is any type that is not a class, interface, or trait.
          if ($type->isBuiltin()) {
            self::$usefulClassTypes[$type->getName()] = FALSE;
          }
        }
      }
    }

    $return = self::fromExplode('|', ltrim($types, '?'))
      ->map(fn($type) => trim($type, '()'));

    if (str_starts_with($types, '?')) {
      $return[] = 'null';
    }

    return $return;
  }

  public function __toString(): string {
    return $this->toString(FALSE);
  }

  /**
   * @param bool $force
   *   Force for documentation, but not for actual typing.
   */
  public function toString(bool $force = TRUE): string {
    if (!$force && $this->isNotDeclarable()) {
      return '';
    }

    if ($this->count() === 1) {
      return $this->reset();
    }

    if ($this->count() === 2 && $this->inArray('null')) {
      $type = $this->clone()->diff(['null'])->reset();

      if (!self::isIntersection($type)) {
        return '?' . $type;
      }
    }

    return $this->clone()
      ->map(fn($type) => self::isIntersection($type) ? "($type)" : $type)
      ->join('|');
  }

  public function isNotDeclarable(): bool {
    return $this->inArray('resource');
  }

  public function isMixed(): bool {
    return $this->inArray('mixed');
  }

  public function isArrayAllowed(): bool {
    return $this->inArray('array')
      || $this->inArray('iterable')
      || $this->isMixed();
  }

  public function isOnlyArrayAllowed(): bool {
    return $this->inArray('array') && ($this->count() === 1 || $this->count() === 2 && $this->inArray('\\' . ObjectArray::class));
  }

  public function isArrayOrUsefulObjectAllowed(): bool {
    if ($this->isArrayAllowed() || $this->inArray('object')) {
      return TRUE;
    }

    foreach ($this as $type) {
      self::$usefulClassTypes[$type] ??= interface_exists($type)
        || class_exists($type) && get_class_vars($type);

      if (self::$usefulClassTypes[$type]) {
        return TRUE;
      }
    }

    return FALSE;
  }

  public function isNoSenseType(): bool {
    return $this->count() === 1 && in_array($this->reset(), ['void', 'true']);
  }

  public function addObjectArrayIfPossible(): self {
    if ($this->inArray('array') && !$this->inArray('object') && !$this->inArray(ObjectArray::class)) {
      $this[] = '\\' . ObjectArray::class;
    }

    return $this;
  }

  private static function isIntersection(string $type): bool {
    return str_contains($type, '&');
  }

}
