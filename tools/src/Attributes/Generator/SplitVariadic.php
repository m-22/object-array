<?php

namespace M22\ObjectArray\Tools\Attributes\Generator;

#[\Attribute(\Attribute::TARGET_METHOD)]
readonly class SplitVariadic {

  public array $params;

  public function __construct(string ...$params) {
    $this->params = $params;
  }

}
