<?php

namespace M22\ObjectArray\Tools\Attributes\Generator;

#[\Attribute(\Attribute::TARGET_METHOD)]
readonly class FixedReturnTypes {

  public function __construct(
    public string $types,
  ) {}

}
