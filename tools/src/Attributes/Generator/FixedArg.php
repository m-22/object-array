<?php

namespace M22\ObjectArray\Tools\Attributes\Generator;

#[\Attribute(\Attribute::TARGET_METHOD | \Attribute::IS_REPEATABLE)]
readonly class FixedArg {

  public function __construct(
    public string $name,
    public mixed $value,
  ) {}

}
