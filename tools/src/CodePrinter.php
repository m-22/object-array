<?php

namespace M22\ObjectArray\Tools;

use Nette\PhpGenerator\Printer;

final class CodePrinter extends Printer {

  public int $wrapLength = 80;
  public string $indentation = '  ';
  public int $linesBetweenMethods = 1;
  public bool $bracesOnNextLine = FALSE;

}
