<?php

namespace M22\ObjectArray\Tools;

use M22\ObjectArray;
use Nette\PhpGenerator\Literal;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\TraitType;

/**
 * Generates real methods for the ObjectArray.
 */
final class MethodsGenerator {

  const string TRAIT_NAMESPACE = 'M22\\ObjectArray';

  const string TRAIT_NAME = 'CoreMethods';

  /**
   * Core PHP modules, lowercased.
   *
   * Call get_loaded_extensions() and remove all non-core extensions.
   *
   * @see https://www.php.net/manual/en/extensions.membership.php#extensions.membership.core
   */
  const array CORE_MODULES = [
    'core',
    'standard',
    'spl',
    'date',
    'hash',
    'json',
    'pcre',
    'random',
    'reflection',
    'zend opcache',
  ];

  /**
   * List of functions to skip.
   *
   * There's no sense to generate methods for these functions, as they are
   * either won't work as expected, or don't accept/return arrays despite the
   * reflected type, or simply don't have sense to be generated.
   */
  const array SKIP_FUNCTIONS = [
    'assert',
    'assert_options',
    'call_user_func',
    'call_user_func_array',
    'compact',
    'extract',
    'forward_static_call',
    'forward_static_call_array',
    'fprintf',
    'func_get_arg',
    'func_get_args',
    'get_debug_type',
    'gettype',
    'iterator_apply',
    'iterator_count',
    'iterator_to_array',
    'json_encode',
    'key_exists',
    'pack',
    'pow',
    'printf',
    'settype',
    'sizeof',
    'sprintf',
    'time_nanosleep',
  ];

  private readonly PhpFile $phpFile;

  private readonly TraitType $trait;

  private string $curMethodName;

  private Func $curFunc;

  private \ReflectionClass $oaRc;

  private \ReflectionClass $traitRc;

  public function __construct() {
    $this->phpFile = new PhpFile();
    $this->phpFile->addComment('@file')
      ->addComment('This file is auto-generated.');

    $namespace = $this->phpFile->addNamespace(self::TRAIT_NAMESPACE);
    $namespace->addUse(ObjectArray::class);
    $namespace->addUse(__NAMESPACE__ . '\\Attributes\\Generator');

    $this->trait = $namespace->addTrait(self::TRAIT_NAME);
    $this->trait->addComment('Provides real methods for the ObjectArray.');
  }

  public static function generate(): void {
    $generator = new self();
    $printer = new CodePrinter();

    $path = __DIR__ . '/../../src/' . self::TRAIT_NAME . '.php';
    file_put_contents($path, $printer->printFile($generator->genFile()));
  }

  public function genFile(): PhpFile {
    foreach (self::getFunctionNames() as $function) {
      $this->genStaticMethod($function);
      $this->genInstanceMethod($function);
    }

    return $this->phpFile;
  }

  /**
   * Generates a static fromFunction() method.
   */
  public function genStaticMethod(string $function_name): void {
    $init = $this->initState($function_name, TRUE);
    $to_gen = $init && $this->curFunc->getReturnTypes()?->isArrayOrUsefulObjectAllowed();

    if ($to_gen) {
      $this->trait->addMethod($this->curMethodName)
        ->setStatic()
        ->setFinal()
        ->setReturnType('static');
      $this->genMethodParamsBodyCommentAttrs();
    }
  }

  /**
   * Generates an instance method.
   */
  public function genInstanceMethod(string $function_name): void {
    $init = $this->initState($function_name, FALSE);
    $to_gen = $init && $this->curFunc->defaultArrayParam
      // Among methods using func_get_args(), generate only those for which
      // the default array parameter is the first one.
      && (!$this->curFunc->getVirtualParams() || !$this->curFunc->defaultArrayParam->position);

    if ($to_gen) {
      $ret_types = $this->curFunc->getReturnTypes();
      $this->trait->addMethod($this->curMethodName)
        ->setFinal()
        ->setReturnType(match (TRUE) {
          $ret_types?->isNoSenseType() => 'static',
          default => str_replace('array', 'static', (string) $ret_types),
        });
      $this->genMethodParamsBodyCommentAttrs();
    }
  }

  public function initState(string $function_name, bool $is_static): bool {
    $this->curMethodName = self::snake2camelCase(match (TRUE) {
      $is_static && str_starts_with($function_name, 'get_') => 'from_' . substr($function_name, 4),
      $is_static => 'from_' . $function_name,
      str_starts_with($function_name, 'array_') => substr($function_name, 6),
      default => $function_name,
    });

    if ($this->methodExists($this->curMethodName)) {
      return FALSE;
    }

    $this->traitRc ??= new \ReflectionClass(self::TRAIT_NAMESPACE . '\\' . self::TRAIT_NAME);
    $this->curFunc = new Func(
      new \ReflectionFunction($function_name),
      $this->traitRc->hasMethod($this->curMethodName)
        ? $this->traitRc->getMethod($this->curMethodName)
        : $is_static,
    );

    if ($this->curFunc->hasInimitableParams) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Checks if the method is defined in the ObjectArray class.
   */
  public function methodExists(string $name): bool {
    $this->oaRc ??= new \ReflectionClass(ObjectArray::class);
    $method = $this->oaRc->hasMethod($name) ? $this->oaRc->getMethod($name) : NULL;
    $exists = $method && $method->getFileName() === $this->oaRc->getFileName();

    if ($exists && !$method->isPublic()) {
      throw new \Exception("Method ObjectArray::$name() exists but it's not public.");
    }

    return $exists;
  }

  public function genMethodParamsBodyCommentAttrs(): void {
    $method = $this->trait->getMethod($this->curMethodName);

    foreach ($this->curFunc->getParamsToDeclare() as $param) {
      $this->genParam($param);
    }

    $this->genMethodBody();
    $this->genMethodComment();

    foreach ($this->curFunc->getAttributes() as $attribute) {
      $method->addAttribute($attribute->getName(), $attribute->getArguments());
    }
  }

  public function genParam(Param $param): void {
    $method = $this->trait->getMethod($this->curMethodName);

    $generated_param = $method->addParameter($param->name)
      ->setReference($param->isPassedByReference())
      ->setType($param->getTypes());

    if ($param->isVariadic()) {
      $method->setVariadic();
    }

    if ($default = $param->getDefaultValueLiteral()) {
      $generated_param->setDefaultValue($default);
    }

    foreach ($param->getAttributes() as $attribute) {
      $generated_param->addAttribute($attribute->getName(), $attribute->getArguments());
    }
  }

  /**
   * Must be called only after the method params are generated.
   */
  public function genMethodComment(): void {
    $method = $this->trait->getMethod($this->curMethodName);
    $func_name = $this->curFunc->name;

    $to_doc_params = $this->curFunc->getVirtualParams()
      || $this->curFunc->hasUntypedParams
      || $this->curFunc->hasParamsWithNotDeclarableTypes;

    if ($to_doc_params) {
      $this->curFunc->hasUntypedParams && $method->addComment('@todo Declare or document parameter type(s).');

      /** @var \M22\ObjectArray\Tools\Param $param */
      foreach ($this->curFunc->getParamsToDeclare() + $this->curFunc->getVirtualParams() as $param) {
        $method->addComment($param->document());
      }

      $method->addComment('');
    }

    $ret_types = $this->curFunc->getReturnTypes();

    if (!$ret_types || $ret_types->isNotDeclarable() && !$method->isStatic()) {
      $ret_types || $method->addComment('@todo Document return type(s).');

      $method->addComment('@return ' . $ret_types?->toString());
      $method->addComment('');
    }

    $method->addComment("@see $func_name()");
  }

  /**
   * Must be called only after the method params are generated.
   */
  public function genMethodBody(): void {
    $method = $this->trait->getMethod($this->curMethodName);
    $arguments = [];
    // Use static::call to convert ObjectArray parameters to arrays.
    $use_call = $this->curFunc->acceptsOtherArrayParams;

    // Gather a list of arguments for the function call.
    if ($this->curFunc->getVirtualParams()) {
      $arguments[] = new Literal('...func_get_args()');

      if (!$method->isStatic()) {
        $arguments[] = new Literal($use_call ? '$this' : '$this->array');
      }
    }
    else {
      foreach (array_reverse($this->curFunc->getParams()) as $name => $param) {
        if ($method->hasParameter($name)) {
          $arguments[] = new Literal(match (TRUE) {
            $param->isVariadic() => '...$?',
            $param->isPassedByReference() && $use_call => '&$?',
            default => '$?',
          }, [$name]);
        }
        // Omit setting trailing arguments with default values.
        elseif ($param->isFixedArg() && ($arguments || !$param->mayBeSkipped())) {
          $arguments[] = $param->getFixedArgValue();
        }

        if ($param === $this->curFunc->defaultArrayParam) {
          $arguments[] = new Literal($use_call ? '$this' : '$this->array');
        }
      }
    }

    $arguments = array_reverse($arguments);

    // Call the function and process the result.
    if ($use_call) {
      // Replace [...$the_only_var] with $the_only_var.
      if (count($arguments) === 1 && $arguments[0] instanceof Literal && str_starts_with($arguments[0], '...')) {
        $value = '?(...), ?';
        $arguments = new Literal(substr($arguments[0], 3));
      }
      else {
        $value = '?(...), [...?]';
      }
    }
    else {
      $value = '?(...?)';
    }

    $arg = new Literal($value, [new Literal($this->curFunc->name), $arguments]);
    $wrappers = [];
    $ret_types = $this->curFunc->getReturnTypes();

    if ($use_call) {
      $wrappers['call'] = 'static::call(?)';
    }

    if ($method->isStatic()) {
      $wrappers['static'] = 'new static(?)';
    }
    elseif ($ret_types?->isOnlyArrayAllowed()) {
      $wrappers['array'] = '$this->setArray(?)';
    }
    elseif (!$ret_types || $ret_types->isArrayAllowed()) {
      unset($wrappers['call']);
      $wrappers['apply'] = $use_call ? '$this->apply(?)' : '$this->applyResult(?)';
    }

    $wrappers['return'] = $method->isStatic() || !$ret_types?->isNoSenseType()
      ? 'return ?;'
      : '?;' . "\n" . 'return $this;';

    foreach ($wrappers as $wrapper) {
      $arg = new Literal($wrapper, [$arg]);
    }

    $method->setBody($arg);
  }

  public static function getFunctionNames(): array {
    $functions_grouped = array_map('get_extension_funcs', self::CORE_MODULES);
    $functions = array_merge(...array_filter($functions_grouped));
    $functions = array_diff($functions, self::SKIP_FUNCTIONS);
    return array_filter(
      $functions,
      fn($fn) => !str_starts_with($fn, 'is_') && !str_ends_with($fn, 'val'),
    );
  }

  /**
   * Converts every underscore with a small letter to a capital letter.
   *
   * E.g., foo_bar => fooBar, _baz => Baz.
   *
   * @see \M22\ObjectArray::camel2snakeCase()
   */
  public static function snake2camelCase(string $string): string {
    return preg_replace_callback(
      '/_([a-z])/',
      fn($match) => strtoupper($match[1]),
      $string,
    );
  }

}
