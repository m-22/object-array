<?php

namespace M22\ObjectArray\Tools;

use M22\ObjectArray\Tools\Attributes\Generator\FixedArg;
use M22\ObjectArray\Tools\Attributes\Generator\FixedReturnTypes;
use M22\ObjectArray\Tools\Attributes\Generator\SplitVariadic;

final class Func {

  public readonly string $name;

  public readonly ?\ReflectionMethod $reflectionMethod;

  public readonly bool $isStatic;

  private Types|false $returnTypes;

  /**
   * @var \M22\ObjectArray\Tools\Param[]
   */
  private array $params = [];

  /**
   * @var \M22\ObjectArray\Tools\Param[]
   */
  private array $paramsToDeclare = [];

  /**
   * @var \M22\ObjectArray\Tools\Param[]
   */
  private array $virtualParams = [];

  public readonly ?Param $defaultArrayParam;

  public readonly bool $acceptsOtherArrayParams;

  public readonly bool $hasInimitableParams;

  public readonly bool $hasUntypedParams;

  public readonly bool $hasParamsWithNotDeclarableTypes;

  private array $fixedArgs;

  private array $methodReflectionParameters;

  public function __construct(
    public readonly \ReflectionFunction $reflectionFunction,
    \ReflectionMethod|bool $reflectionMethodOrIsStatic,
  ) {
    $this->name = $this->reflectionFunction->name;

    if (is_bool($reflectionMethodOrIsStatic)) {
      $this->isStatic = $reflectionMethodOrIsStatic;
      $this->reflectionMethod = NULL;
    }
    else {
      $this->reflectionMethod = $reflectionMethodOrIsStatic;
      $this->isStatic = $this->reflectionMethod->isStatic();
    }

    $this->constructParams();
  }

  private function constructParams(): void {
    $accepts_other_array_params
      = $has_inimitable_params
      = $has_untyped_params
      = $has_not_declarable_types
      = FALSE;

    foreach ($this->reflectionFunction->getParameters() as $ref) {
      $param = $this->params[$ref->name] = new Param($ref, $this);
      $has_inimitable_params |= $param->isInimitable();
    }

    // Check for the split variadic.
    $last = end($this->params);
    $rest = $last && $last->isVariadic() ? $this->getSplitVariadicParams() : NULL;

    if ($rest) {
      unset($this->params[$last->name]);

      foreach ($rest as $name) {
        $this->params[$name] = new Param($this->getMethodReflectionParameter($name), $this);
      }
    }

    // Define the default array parameter.
    $priorities = !$this->isStatic ? array_filter(array_map(
      fn($param) => $param->canBeDefaultArrayParam(),
      $this->params,
    )) : [];

    $this->defaultArrayParam = $priorities
      ? $this->params[array_search(max($priorities), $priorities, TRUE)]
      : NULL;

    foreach ($this->params as $name => $param) {
      if (!$param->isFixedArg() && ($this->isStatic || $param !== $this->defaultArrayParam || $param->isVariadic())) {
        $types = $param->getTypes();

        if (!$param->isOptionalWithoutDefault() && !$this->virtualParams) {
          $this->paramsToDeclare[$name] = $param;
          $has_untyped_params |= !$types;
          $has_not_declarable_types |= $types?->isNotDeclarable();
        }
        else {
          $this->virtualParams[$name] = $param;
        }

        $accepts_other_array_params |= !$types || $types->isArrayAllowed();
      }
    }

    // Sort params to declare.
    if ($this->paramsToDeclare && $rest) {
      $positions = [];

      foreach ($this->paramsToDeclare as $name => $param) {
        $existing = $this->getMethodReflectionParameter($name);
        $positions[$name] = match (TRUE) {
          $param->isVariadic() => PHP_INT_MAX,
          !empty($existing) => PHP_INT_MIN + $existing->getPosition(),
          default => $param->position,
        };
      }

      asort($positions);
      $this->paramsToDeclare = array_merge($positions, $this->paramsToDeclare);
    }

    $this->acceptsOtherArrayParams = $accepts_other_array_params;
    $this->hasInimitableParams = $has_inimitable_params;
    $this->hasUntypedParams = $has_untyped_params;
    $this->hasParamsWithNotDeclarableTypes = $has_not_declarable_types;
  }

  public function getMethodReflectionParameter(string $name): ?\ReflectionParameter {
    if (!isset($this->methodReflectionParameters)) {
      $this->methodReflectionParameters = $this->reflectionMethod
        ? array_column($this->reflectionMethod->getParameters(), NULL, 'name')
        : [];
    }

    return $this->methodReflectionParameters[$name] ?? NULL;
  }

  /**
   * @return \M22\ObjectArray\Tools\Param[]
   *   Assoc array of Param instances, keyed by param name.
   */
  public function getParams(): array {
    return $this->params;
  }

  public function getParamsToDeclare(): array {
    return $this->paramsToDeclare;
  }

  public function getVirtualParams(): array {
    return $this->virtualParams;
  }

  public function getReturnTypes(): ?Types {
    if (!isset($this->returnTypes)) {
      $attrs = $this->getAttributes(FixedReturnTypes::class);
      $raw_types = $attrs
        ? reset($attrs)->newInstance()->types
        : $this->reflectionFunction->getReturnType() ?? $this->getReturnTypesFromMethodDocComment();

      $this->returnTypes = $raw_types
        ? Types::fromStringable($raw_types)
        : FALSE;
    }

    return $this->returnTypes ?: NULL;
  }

  public function getReturnTypesFromMethodDocComment(): ?string {
    if ($doc = $this->reflectionMethod?->getDocComment()) {
      if (preg_match('/@return (?<types>\S+)\b/', $doc, $matches)) {
        return $matches['types'];
      }
    }

    return NULL;
  }

  /**
   * @return \ReflectionAttribute[]
   */
  public function getAttributes(?string $name = NULL): array {
    return $this->reflectionMethod?->getAttributes($name) ?? [];
  }

  public function getFixedArgs(): array {
    return $this->fixedArgs ??= array_column(array_map(
      fn($attribute) => $attribute->newInstance(),
      $this->getAttributes(FixedArg::class),
    ), 'value', 'name');
  }

  public function getSplitVariadicParams(): ?array {
    $attrs = $this->getAttributes(SplitVariadic::class);
    return $attrs ? reset($attrs)->newInstance()->params : NULL;
  }

}
