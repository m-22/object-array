<?php

namespace M22\ObjectArray\Tests\Integration;

use M22\ObjectArray as A;
use M22\ObjectArray\Tests\TestBase;
use PHPUnit\Framework\Attributes\DataProvider;

final class CoreMethodsTest extends TestBase {

  public static function staticMethodsProvider(): array {
    return [
      [explode(...), A::fromExplode(...), ['/', 'm22/object-array/src/ObjectArray.php']],
      [range(...), A::fromRange(...), ['a', 'z']],
      [str_split(...), A::fromStrSplit(...), ['m22/object-array/src/ObjectArray.php', 3]],
    ];
  }

  #[DataProvider('staticMethodsProvider')]
  public function testStaticMethods(callable $fn, callable $method, array $args): void {
    $expected = $fn(...$args);
    $a = $method(...$args);
    $this->assertSame($expected, $a->array);
  }

  #[DataProvider('constructorProvider')]
  public function testFromReset(array $expected, array|object $array_or_object): void {
    $array = [$array_or_object];
    $a = A::fromReset($array);
    $this->assertSame($expected, $a->array);
  }

  public function testFromResetWithAnotherInstance(): void {
    $a = new A(static::xyz());
    $array = [$a];
    $b = A::fromReset($array);
    $this->assertSame(static::xyz(), $b->array);
    $this->assertTrue($b !== $a);
  }

  public function testInArray(): void {
    $a = new A(static::xyz());
    $this->assertTrue($a->inArray(2));
    $this->assertFalse($a->inArray(4));
    $this->assertFalse($a->inArray('2', TRUE));
  }

  public function testImplode(): void {
    $a = new A(static::xyz());
    $this->assertSame('321', $a->implode());
    $this->assertSame('321', $a->join());
    $this->assertSame('3--2--1', $a->implode('--'));
    $this->assertSame('3..2..1', $a->join('..'));
  }

  public function testMap(): void {
    $a = new A(static::xyz());
    $squared = ['x' => 9, 'y' => 4, 'z' => 1];

    $this->assertObjectArrayResult($a, $a->map(fn($i) => $i * $i), $squared);
    $this->assertSame($squared, $a->map(NULL)->array);

    $a->map(fn($i, $x) => $i . $x, new A(array_keys($squared)));
    $this->assertSame(['9x', '4y', '1z'], $a->array);

    $array0 = $a->array;
    $array1 = static::xyzFlipped();
    $a->map(NULL, $array1);
    $this->assertSame(array_map(NULL, $array0, $array1), $a->array);
  }

  public function testReferences(): void {
    $array = static::xyzWithArrays();
    $b = A::fromEnd($array);

    $this->assertSame([1, 2, 3], $b->array);
    $this->assertIsArray($array);
    $this->assertSame('z', key($array));

    $a = new A(static::xyzWithArrays());
    $b = A::fromEnd($a);

    $this->assertSame([1, 2, 3], $b->array);
    $this->assertInstanceOf(A::class, $a);
    $this->assertSame('z', key($a->array));

    $a = new A(['abcde']);
    $this->assertObjectArrayResult(
      $a,
      $a->strReplace(range('b', 'd'), '-', $count),
      ['a---e'],
    );
    $this->assertSame(3, $count);

    $a = new A(static::xyz());
    $b = A::fromArraySplice($a, 1, 1, ['a', 'b']);

    $this->assertSame(['x' => 3, 'a', 'b', 'z' => 1], $a->array);
    $this->assertSame(['y' => 2], $b->array);

    $a = new A(static::xyz());
    $this->assertObjectArrayResult(
      $a,
      $a->splice(1, 1, ['a', 'b']),
      ['y' => 2],
    );
  }

  public function testSorts(): void {
    $a = new A(static::xyz());
    $this->assertSame(static::xyzReversed(), $a->asort()->array);
    $this->assertSame(static::xyz(), $a->arsort()->array);
    $this->assertSame(static::xyzReversed(), $a->krsort()->array);
    $this->assertSame(static::xyz(), $a->ksort()->array);
    $this->assertSame(static::xyzReversed(), $a->uasort('strcmp')->array);
    $this->assertSame(static::xyz(), $a->uksort('strcmp')->array);
    $this->assertSame([1, 2, 3], $a->usort('strcmp')->array);
    $this->assertSame([3, 2, 1], $a->rsort()->array);
    $this->assertSame([1, 2, 3], $a->sort()->array);
  }

  public function testNatsort(): void {
    $array = ['img12.png', 'img10.png', 'img2.png', 'img1.png'];
    $a = new A($array);

    natsort($array);
    $this->assertObjectArrayResult($a, $a->natsort(), $array);

    $array = ['IMG0.png', 'img12.png', 'img10.png', 'img2.png', 'IMG3.png'];
    $a = new A($array);

    natcasesort($array);
    $this->assertObjectArrayResult($a, $a->natcasesort(), $array);
  }

  public function testShuffle(): void {
    $range = range(1, 100);
    $a = new A($range);

    $this->assertSame($a, $a->shuffle());
    $this->assertNotEquals($range, $a->array);
    $this->assertSame($range, $a->sort()->array);
  }

  public function testGeneratedMethodsWithSkippedParams(): void {
    $a = new A(static::xyz());
    $this->assertObjectArrayResult($a, $a->keys(), ['x', 'y', 'z']);

    $b = A::fromArrayKeys(static::xyz());
    $this->assertSame(['x', 'y', 'z'], $b->array);

    $array = ['x' => 3, 'y' => 2, 'z' => 1, 'a' => '2'];

    $a = new A($array);
    $this->assertObjectArrayResult($a, $a->keys(2), ['y', 'a']);

    $b = A::fromArrayKeys($array, '2');
    $this->assertSame(['y', 'a'], $b->array);

    $a = new A($array);
    $this->assertObjectArrayResult($a, $a->keys(2, TRUE), ['y']);

    $b = A::fromArrayKeys($array, '2', TRUE);
    $this->assertSame(['a'], $b->array);

    $a = new A(static::xyz());
    $this->assertObjectArrayResult(
      $a,
      $a->walk(fn(&$value, $key) => $value = $value . $key),
      ['x' => '3x', 'y' => '2y', 'z' => '1z'],
    );

    $a = new A(static::xyz());
    $this->assertObjectArrayResult(
      $a,
      $a->walk(
        fn(&$value, $key, $extra) => $value = $value . $key . (is_array($extra) ? 1 : 0),
        new A([]),
      ),
      ['x' => '3x1', 'y' => '2y1', 'z' => '1z1'],
    );

    $a = new A(static::xyz());
    $a->walkRecursive(fn(&$value, $key) => $value = $value . $key);
    $this->assertSame(['x' => '3x', 'y' => '2y', 'z' => '1z'], $a->array);

    $a = new A(static::xyz());
    $a->walkRecursive(
      fn(&$value, $key, $extra) => $value = $value . $key . (is_array($extra) ? 1 : 0),
      new A([]),
    );
    $this->assertSame(['x' => '3x1', 'y' => '2y1', 'z' => '1z1'], $a->array);
  }

}
