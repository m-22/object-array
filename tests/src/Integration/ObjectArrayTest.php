<?php

namespace M22\ObjectArray\Tests\Integration;

use M22\ObjectArray as A;
use M22\ObjectArray\Tests\TestBase;

final class ObjectArrayTest extends TestBase {

  public static function setUpBeforeClass(): void {
    require_once __DIR__ . '/../../fixtures/custom_functions.php';
  }

  public function testMagicCallStatic(): void {
    // This also tests casting an ObjectArray argument to array.
    $a = A::fromCustomArrayFlip1('', array: new A(static::xyz()));
    $this->assertSame(static::xyzFlipped(), $a->array);

    $a = A::fromCustomArray(static::xyz());
    $this->assertSame(static::xyz(), $a->array);
  }

  public function testMagicCall(): void {
    $a = new A(static::xyz());
    $this->assertTrue($a->customAssertCount1(3));
  }

  public function testMagicCallCasting(): void {
    $a = new A(static::xyz());
    $this->assertObjectArrayResult($a, $a->customDiff1(new A([3, 1])), ['y' => 2]);
  }

  public function testCallStaticWithParamByRef(): void {
    $a = new A(static::xyz());
    $b = A::fromCustomWithParamByRef1($a);
    $this->assertInstanceOf(A::class, $a);
    $this->assertSame([...static::xyz(), 'a'], $a->array);
    $this->assertSame(['c'], $b->array);
  }

  public function testCallWithParamByRef(): void {
    $a = new A(static::xyz());
    $b = new A(static::xyzReversed());
    $a->customWithParamByRef1($b);
    $this->assertInstanceOf(A::class, $b);
    $this->assertSame(['c'], $a->array);
    $this->assertSame([...static::xyzReversed(), 'b'], $b->array);
  }

}
