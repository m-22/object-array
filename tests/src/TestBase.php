<?php

namespace M22\ObjectArray\Tests;

use M22\ObjectArray as A;
use PHPUnit\Framework\TestCase;

abstract class TestBase extends TestCase {

  final protected function assertObjectArrayResult(A $initial, mixed $resulting, array $expected_array): void {
    $this->assertSame($initial, $resulting);
    $this->assertSame($expected_array, $initial->array);
  }

  final protected static function xyz(): array {
    return ['x' => 3, 'y' => 2, 'z' => 1];
  }

  final protected static function xyzFlipped(): array {
    return [3 => 'x', 2 => 'y', 1 => 'z'];
  }

  final protected static function xyzReversed(): array {
    return ['z' => 1, 'y' => 2, 'x' => 3];
  }

  final protected static function xyzWithArrays(): array {
    return [
      'x' => [1],
      'y' => [1, 2],
      'z' => [1, 2, 3],
    ];
  }

  public static function constructorProvider(): array {
    return [
      'empty array'       => [[], []],
      'empty ArrayObject' => [[], new \ArrayObject()],
      'empty Traversable' => [[], new \ArrayIterator()],
      'empty object'      => [[], new \stdClass()],

      'array'       => [static::xyz(), static::xyz()],
      'ArrayObject' => [static::xyz(), new \ArrayObject(static::xyz())],
      'Traversable' => [static::xyz(), new \ArrayIterator(static::xyz())],
      'object'      => [static::xyz(), (object) static::xyz()],
    ];
  }

}
