<?php

namespace M22\ObjectArray\Tests\Unit;

use M22\ObjectArray as A;
use M22\ObjectArray\Tests\TestBase;
use PHPUnit\Framework\Attributes\DataProvider;

final class ObjectArrayTest extends TestBase {

  public function testEmptyConstructor(): void {
    $a = new A();
    $this->assertSame([], $a->array);
    $this->assertTrue($a->isEmpty());
  }

  #[DataProvider('constructorProvider')]
  public function testConstructor(array $expected, array|object $array_or_object): void {
    $a = new A($array_or_object);
    $this->assertSame($expected, $a->array);
    $this->assertSame(!count($expected), $a->isEmpty());
  }

  #[DataProvider('constructorProvider')]
  public function testFrom(array $expected, array|object $array_or_object): void {
    $a = A::from($array_or_object);
    $this->assertSame($expected, $a->array);
  }

  public function testConstructingFromAnotherInstance(): void {
    $a = new A(static::xyz());
    $b = new A($a);
    $this->assertSame(static::xyz(), $b->array);
  }

  public function testFromAnotherInstance(): void {
    $a = new A(static::xyz());
    $b = A::from($a);
    $this->assertSame(static::xyz(), $b->array);
    $this->assertTrue($b !== $a);
  }

  public function testCallingNotExistingStatic(): void {
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Call to undefined method');
    A::notExistingFunction();
  }

  public function testCallingNotExistingInstance(): void {
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Call to undefined method');
    A::from([])->notExistingFunction();
  }

  public function testMagicGetterSetter(): void {
    $a = new A(static::xyz());
    $this->assertTrue(isset($a->x));
    $this->assertSame(3, $a->x);

    $a->x = NULL;
    $this->assertFalse(isset($a->x));
    $this->assertArrayHasKey('x', $a->array);

    unset($a->x);
    $this->assertArrayNotHasKey('x', $a->array);

    $this->assertFalse(isset($a->w));
    $a->w = 4;
    $this->assertTrue(isset($a->w));
    $this->assertSame(4, $a->w);
    $this->assertSame(4, $a->array['w']);
  }

  public function testArrayAccess(): void {
    $a = new A(static::xyz());
    $this->assertTrue(isset($a['x']));
    $this->assertSame(3, $a['x']);

    $a['x'] = NULL;
    $this->assertFalse(isset($a['x']));
    $this->assertArrayHasKey('x', $a->array);

    unset($a['x']);
    $this->assertArrayNotHasKey('x', $a->array);

    $this->assertFalse(isset($a['w']));
    $a['w'] = 4;
    $this->assertTrue(isset($a['w']));
    $this->assertSame(4, $a['w']);
    $this->assertSame(4, $a->array['w']);

    $a[] = 'zero';
    $a[] = 'one';
    $this->assertSame(['zero', 'one'], array_slice($a->array, -2, preserve_keys: TRUE));

    $a['w']++;
    $this->assertSame(5, $a['w']);
    $this->assertSame(4, --$a['w']);

    // Test destructuring (ArrayAccess feature).
    [1 => $one, 'y' => $y] = $a;
    $this->assertSame('one', $one);
    $this->assertSame(2, $y);
  }

  public function testCount(): void {
    $a = new A(static::xyz());
    $this->assertSame(3, count($a));
  }

  public static function jsonSerializeProvider(): array {
    return [
      'empty' => [[]],
      'assoc' => [static::xyz()],
      'indexed, not consecutive' => [static::xyzFlipped()],
      'list' => [['x', 'y', 'z']],
    ];
  }

  #[DataProvider('jsonSerializeProvider')]
  public function testJsonSerialize(array $array): void {
    $a = new A($array);
    $this->assertSame(json_encode($array), json_encode($a));
  }

  public function testTraversable(): void {
    $a = new A(static::xyz());
    $array = [];

    foreach ($a as $key => $value) {
      $array[$key] = $value;
    }

    $this->assertSame(static::xyz(), $array);

    // Test array unpacking.
    $a = new A(static::xyz());
    $this->assertSame(static::xyz(), [...$a]);

    $a = new A(static::xyzFlipped());
    // Numeric indices are reordered.
    $this->assertSame(['x', 'y', 'z'], [...$a]);
  }

  public function testClone(): void {
    $a = new A(static::xyz());
    $b = $a->clone();

    $this->assertSame(static::xyz(), $b->array);
    $this->assertNotSame($a, $b);
  }

  public function testPlus(): void {
    $a = new A(static::xyz());
    $b = new A(['v' => 5, 'w' => 5]);

    $this->assertObjectArrayResult(
      $a,
      $a->plus(['y' => 4, 'w' => 4], $b),
      [...static::xyz(), 'w' => 4, 'v' => 5],
    );
    $this->assertInstanceOf(A::class, $b);
    $this->assertSame(['v' => 5, 'w' => 5], $b->array);
  }

  public function testPointerRelated(): void {
    $a = new A();
    $this->assertNull($a->reset());
    $this->assertNull($a->next());
    $this->assertNull($a->current());
    $this->assertNull($a->pos());
    $this->assertNull($a->prev());
    $this->assertNull($a->end());

    $a = new A(static::xyzReversed());
    $this->assertSame(3, $a->end());

    $this->assertSame(3, current($a->array));
    $this->assertSame(3, $a->current());

    $this->assertSame(2, $a->prev());
    $this->assertSame(3, $a->next());
    $this->assertNull($a->next());

    $this->assertSame(1, $a->reset());
    $this->assertSame(1, $a->pos());

    $this->assertNull($a->prev());

    // Test array replacement with a function result.
    $a = new A(static::xyzWithArrays());
    $this->assertObjectArrayResult($a, $a->end(), [1, 2, 3]);

    $a = new A([1, 2, 3]);
    $b = new A([new A([1]), $a]);
    $this->assertObjectArrayResult($b, $b->end(), [1, 2, 3]);
    $this->assertInstanceOf(A::class, $a);
    $this->assertNotSame($b, $a);

    $b->array[0] = 0;
    $this->assertSame([1, 2, 3], $a->array);
  }

  public static function reindexProvider(): array {
    return [
      'integer index key' => [5, [[5 => 'b'], [5 => 'a'], [5 => 'c']]],
      'string index key' => ['i', [['i' => 1], ['i' => 2], ['i' => 0]]],
    ];
  }

  #[DataProvider('reindexProvider')]
  public function testReindex(int|string $index_key, array $array): void {
    $a = new A($array);
    $this->assertObjectArrayResult($a, $a->reindex($index_key), array_column($array, NULL, $index_key));
  }

  public function testCombineWithSelf(): void {
    $a = new A(static::xyz());
    $this->assertObjectArrayResult($a, $a->combineWithSelf(), array_combine(static::xyz(), static::xyz()));
  }

}
