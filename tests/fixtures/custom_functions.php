<?php

/**
 * @file
 * Provides custom functions for the testing purposes.
 */

function custom_array_flip1(
  string $string = '',
  bool $bool = FALSE,
  array $array = [],
): array {
  return array_flip($array);
}

function array_custom_assert_count1(array $array, int $count): bool {
  return count($array) === $count;
}

function custom_assert_count1(): never {
  throw new \LogicException('This function must never be called.');
}

function get_custom_array(array $array): array {
  return $array;
}

function custom_array(): never {
  throw new \LogicException('This function must never be called.');
}

function array_custom_diff1(array $array, array $array1): array {
  return array_diff($array, $array1);
}

function custom_with_param_by_ref1(array &$array1, array &$array2 = []): array {
  $array1[] = 'a';
  $array2[] = 'b';
  return ['c'];
}
