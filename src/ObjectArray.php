<?php

namespace M22;

use M22\ObjectArray\CoreMethods;

/**
 * Allows arrays to work as objects.
 */
class ObjectArray implements \IteratorAggregate, \ArrayAccess, \Countable, \JsonSerializable {

  use CoreMethods;

  public array $array;

  private static array $fnNames = [];

  /**
   * Constructs a new ObjectArray.
   *
   * @param array|ObjectArray|\ArrayObject|\Traversable|object $array_or_object
   */
  public function __construct(array|object $array_or_object = []) {
    $this->array = match (TRUE) {
      is_array($array_or_object) => $array_or_object,
      $array_or_object instanceof ObjectArray => $array_or_object->array,
      $array_or_object instanceof \ArrayObject => $array_or_object->getArrayCopy(),
      $array_or_object instanceof \Traversable => iterator_to_array($array_or_object),
      is_object($array_or_object) => get_object_vars($array_or_object),
    };
  }

  public static function from(array|object $array_or_object): static {
    return new static($array_or_object);
  }

  /**
   * Creates a new ObjectArray from a function call result.
   *
   * Magic methods pass parameters by value only. In order to call a function
   * with an array parameter by reference, an ObjectArray instance should be
   * passed to this method instead of an array.
   *
   * @param string $name
   *   Name of the function to call, prefixed with 'from'. A camel-cased
   *   fromFunctionName is converted to function_name.
   * @param array $arguments
   *   List of function arguments.
   *
   * @return static
   */
  public static function __callStatic(string $name, array $arguments): static {
    if ($fn = static::getFnName($name, TRUE)) {
      return new static(static::call($fn, $arguments));
    }

    throw new \BadMethodCallException('Call to undefined method ' . static::class . '::' . $name . '().');
  }

  /**
   * Calls a function supplying internal array as the 1st parameter.
   *
   * Magic methods pass parameters by value only. In order to call a function
   * with an array parameter by reference, an ObjectArray instance should be
   * passed to this method instead of an array.
   *
   * @param string $name
   *   Name of the function to call. A camel-cased functionName is converted to
   *   function_name.
   * @param array $arguments
   *   List of function arguments. This object's internal array is prepended to
   *   the list.
   *
   * @return $this|mixed
   *   If the function returns an array or an ObjectArray, this method returns
   *   this object for chaining, replacing internal array with a function call
   *   result. Otherwise, a function call result is returned.
   */
  public function __call(string $name, array $arguments): mixed {
    if ($fn = static::getFnName($name, FALSE)) {
      return $this->apply($fn, [$this, ...$arguments]);
    }

    throw new \BadMethodCallException('Call to undefined method ' . static::class . '::' . $name . '().');
  }

  public function __isset(string $name): bool {
    return $this->offsetExists($name);
  }

  public function __get(string $name): mixed {
    return $this->offsetGet($name);
  }

  public function __set(string $name, mixed $value): void {
    $this->offsetSet($name, $value);
  }

  public function __unset(string $name): void {
    $this->offsetUnset($name);
  }

  public function offsetExists(mixed $offset): bool {
    return isset($this->array[$offset]);
  }

  public function &offsetGet(mixed $offset): mixed {
    return $this->array[$offset];
  }

  public function offsetSet(mixed $offset, mixed $value): void {
    if (is_null($offset)) {
      $this->array[] = $value;
    }
    else {
      $this->array[$offset] = $value;
    }
  }

  public function offsetUnset(mixed $offset): void {
    unset($this->array[$offset]);
  }

  public function count(): int {
    return count($this->array);
  }

  public function jsonSerialize(): array {
    return $this->array;
  }

  public function getIterator(): \ArrayIterator {
    // Always instantiate new iterator, because the array can change between
    // calls to this method.
    return new \ArrayIterator($this->array);
  }

  public function isEmpty(): bool {
    return empty($this->array);
  }

  public function clone(): static {
    return clone $this;
  }

  protected function setArray(array $array): static {
    $this->array = $array;
    return $this;
  }

  private static function getFnName(string $method_name, bool $is_static_context): string|FALSE {
    if (!isset(static::$fnNames[$is_static_context][$method_name])) {
      $fn = FALSE;
      $name = static::camel2snakeCase($method_name);

      if (!$is_static_context) {
        if (function_exists('array_' . $name)) {
          $fn = 'array_' . $name;
        }
        elseif (function_exists($name)) {
          $fn = $name;
        }
      }
      elseif (str_starts_with($name, 'from_')) {
        $name = substr($name, 5);

        if (function_exists('get_' . $name)) {
          $fn = 'get_' . $name;
        }
        elseif (function_exists($name)) {
          $fn = $name;
        }
      }

      static::$fnNames[$is_static_context][$method_name] = $fn;
    }

    return static::$fnNames[$is_static_context][$method_name];
  }

  /**
   * Converts every capital letter to an underscore with a small letter.
   *
   * E.g., fooBar => foo_bar, Baz => _baz.
   */
  private static function camel2snakeCase(string $string): string {
    return strtolower(preg_replace('/([A-Z])/', '_$1', $string));
  }

  /**
   * Replaces ObjectArray instances with their internal arrays.
   *
   * @param array $vars
   *   Array of variables to replace.
   *
   * @return array
   *   Replaced variables. Type of variables is anything but ObjectArray.
   */
  final protected static function cast(array $vars): array {
    foreach ($vars as $i => $var) {
      if ($var instanceof ObjectArray) {
        $vars[$i] = &$var->array;
      }
    }

    return $vars;
  }

  /**
   * Wraps callback, casting ObjectArray instances to arrays.
   */
  final protected static function call(callable $callback, array $arguments): mixed {
    return $callback(...static::cast($arguments));
  }

  /**
   * Applies a callback if it returns an array, otherwise returns a result.
   */
  final protected function apply(callable $callback, array $arguments): mixed {
    return $this->applyResult(static::call($callback, $arguments));
  }

  final protected function applyResult(mixed $result): mixed {
    $result = static::cast([$result])[0];
    return is_array($result) ? $this->setArray($result) : $result;
  }

  final public function plus(array|ObjectArray $array, array|ObjectArray ...$arrays): static {
    foreach (static::cast([$array, ...$arrays]) as $array) {
      $this->array += $array;
    }

    return $this;
  }

  final public function end(): mixed {
    return $this->array ? $this->apply(end(...), [$this]) : NULL;
  }

  final public function reset(): mixed {
    return $this->array ? $this->apply(reset(...), [$this]) : NULL;
  }

  final public function next(): mixed {
    $result = $this->apply(next(...), [$this]);
    return $result !== FALSE || key($this->array) !== NULL ? $result : NULL;
  }

  final public function prev(): mixed {
    $result = $this->apply(prev(...), [$this]);
    return $result !== FALSE || key($this->array) !== NULL ? $result : NULL;
  }

  final public function current(): mixed {
    return key($this->array) !== NULL ? $this->apply(current(...), [$this]) : NULL;
  }

  final public function pos(): mixed {
    return $this->current();
  }

  final public function reindex(int|string $index_key): static {
    return $this->setArray(array_column($this->array, NULL, $index_key));
  }

  final public function combineWithSelf(): static {
    return $this->setArray(array_combine($this->array, $this->array));
  }

}
