<?php

/**
 * @file
 * This file is auto-generated.
 */

namespace M22\ObjectArray;

use M22\ObjectArray;
use M22\ObjectArray\Tools\Attributes\Generator;

/**
 * Provides real methods for the ObjectArray.
 */
trait CoreMethods
{
  /**
   * @see define()
   */
  #[Generator\FixedArg('case_insensitive', false)]
  final public function define(string $constant_name): bool {
    return define($constant_name, $this->array);
  }

  /**
   * @see get_class_vars()
   */
  final public static function fromClassVars(string $class): static {
    return new static(get_class_vars($class));
  }

  /**
   * @see get_object_vars()
   */
  final public static function fromObjectVars(object $object): static {
    return new static(get_object_vars($object));
  }

  /**
   * @see get_mangled_object_vars()
   */
  final public static function fromMangledObjectVars(object $object): static {
    return new static(get_mangled_object_vars($object));
  }

  /**
   * @see get_class_methods()
   */
  final public static function fromClassMethods(
    object|string $object_or_class,
  ): static {
    return new static(get_class_methods($object_or_class));
  }

  /**
   * @see get_included_files()
   */
  final public static function fromIncludedFiles(): static {
    return new static(get_included_files());
  }

  /**
   * @see get_required_files()
   */
  final public static function fromRequiredFiles(): static {
    return new static(get_required_files());
  }

  /**
   * @see get_declared_classes()
   */
  final public static function fromDeclaredClasses(): static {
    return new static(get_declared_classes());
  }

  /**
   * @see get_declared_traits()
   */
  final public static function fromDeclaredTraits(): static {
    return new static(get_declared_traits());
  }

  /**
   * @see get_declared_interfaces()
   */
  final public static function fromDeclaredInterfaces(): static {
    return new static(get_declared_interfaces());
  }

  /**
   * @see get_defined_functions()
   */
  final public static function fromDefinedFunctions(
    bool $exclude_disabled = true,
  ): static {
    return new static(get_defined_functions($exclude_disabled));
  }

  /**
   * @see get_defined_vars()
   */
  final public static function fromDefinedVars(): static {
    return new static(get_defined_vars());
  }

  /**
   * @see get_resources()
   */
  final public static function fromResources(?string $type = null): static {
    return new static(get_resources($type));
  }

  /**
   * @see get_loaded_extensions()
   */
  final public static function fromLoadedExtensions(
    bool $zend_extensions = false,
  ): static {
    return new static(get_loaded_extensions($zend_extensions));
  }

  /**
   * @see get_defined_constants()
   */
  final public static function fromDefinedConstants(
    bool $categorize = false,
  ): static {
    return new static(get_defined_constants($categorize));
  }

  /**
   * @see debug_backtrace()
   */
  final public static function fromDebugBacktrace(
    int $options = \DEBUG_BACKTRACE_PROVIDE_OBJECT,
    int $limit = 0,
  ): static {
    return new static(debug_backtrace($options, $limit));
  }

  /**
   * @see get_extension_funcs()
   */
  final public static function fromExtensionFuncs(string $extension): static {
    return new static(get_extension_funcs($extension));
  }

  /**
   * @see gc_status()
   */
  final public static function fromGcStatus(): static {
    return new static(gc_status());
  }

  /**
   * @see ob_list_handlers()
   */
  final public static function fromObListHandlers(): static {
    return new static(ob_list_handlers());
  }

  /**
   * @see ob_get_status()
   */
  final public static function fromObGetStatus(
    bool $full_status = false,
  ): static {
    return new static(ob_get_status($full_status));
  }

  /**
   * @see array_push()
   */
  final public function push(mixed ...$values): int {
    return static::call(array_push(...), [$this, ...$values]);
  }

  /**
   * @see krsort()
   */
  final public function krsort(int $flags = \SORT_REGULAR): static {
    krsort($this->array, $flags);
    return $this;
  }

  /**
   * @see ksort()
   */
  final public function ksort(int $flags = \SORT_REGULAR): static {
    ksort($this->array, $flags);
    return $this;
  }

  /**
   * @see natsort()
   */
  final public function natsort(): static {
    natsort($this->array);
    return $this;
  }

  /**
   * @see natcasesort()
   */
  final public function natcasesort(): static {
    natcasesort($this->array);
    return $this;
  }

  /**
   * @see asort()
   */
  final public function asort(int $flags = \SORT_REGULAR): static {
    asort($this->array, $flags);
    return $this;
  }

  /**
   * @see arsort()
   */
  final public function arsort(int $flags = \SORT_REGULAR): static {
    arsort($this->array, $flags);
    return $this;
  }

  /**
   * @see sort()
   */
  final public function sort(int $flags = \SORT_REGULAR): static {
    sort($this->array, $flags);
    return $this;
  }

  /**
   * @see rsort()
   */
  final public function rsort(int $flags = \SORT_REGULAR): static {
    rsort($this->array, $flags);
    return $this;
  }

  /**
   * @see usort()
   */
  final public function usort(callable $callback): static {
    usort($this->array, $callback);
    return $this;
  }

  /**
   * @see uasort()
   */
  final public function uasort(callable $callback): static {
    uasort($this->array, $callback);
    return $this;
  }

  /**
   * @see uksort()
   */
  final public function uksort(callable $callback): static {
    uksort($this->array, $callback);
    return $this;
  }

  /**
   * @see end()
   */
  final public static function fromEnd(object|array &$array): static {
    return new static(static::call(end(...), [&$array]));
  }

  /**
   * @see prev()
   */
  final public static function fromPrev(object|array &$array): static {
    return new static(static::call(prev(...), [&$array]));
  }

  /**
   * @see next()
   */
  final public static function fromNext(object|array &$array): static {
    return new static(static::call(next(...), [&$array]));
  }

  /**
   * @see reset()
   */
  final public static function fromReset(object|array &$array): static {
    return new static(static::call(reset(...), [&$array]));
  }

  /**
   * @see current()
   */
  final public static function fromCurrent(object|array $array): static {
    return new static(static::call(current(...), [$array]));
  }

  /**
   * @see pos()
   */
  final public static function fromPos(object|array $array): static {
    return new static(static::call(pos(...), [$array]));
  }

  /**
   * @see key()
   */
  final public function key(): string|int|null {
    return key($this->array);
  }

  /**
   * @see min()
   */
  final public static function fromMin(mixed $value, mixed ...$values): static {
    return new static(static::call(min(...), [$value, ...$values]));
  }

  /**
   * @see min()
   */
  final public function min(
    #[Generator\FixedTypes]
    ObjectArray|array ...$values,
  ): mixed {
    return $this->apply(min(...), [$this, ...$values]);
  }

  /**
   * @see max()
   */
  final public static function fromMax(mixed $value, mixed ...$values): static {
    return new static(static::call(max(...), [$value, ...$values]));
  }

  /**
   * @see max()
   */
  final public function max(
    #[Generator\FixedTypes]
    ObjectArray|array ...$values,
  ): mixed {
    return $this->apply(max(...), [$this, ...$values]);
  }

  /**
   * @param callable $callback
   * @param mixed $arg (optional)
   *
   * @see array_walk()
   */
  final public function walk(callable $callback): static {
    static::call(array_walk(...), [$this, ...func_get_args()]);
    return $this;
  }

  /**
   * @param callable $callback
   * @param mixed $arg (optional)
   *
   * @see array_walk_recursive()
   */
  final public function walkRecursive(callable $callback): static {
    static::call(array_walk_recursive(...), [$this, ...func_get_args()]);
    return $this;
  }

  /**
   * @see in_array()
   */
  final public function inArray(
    mixed $needle,
    #[Generator\FixedDefault]
    bool $strict = true,
  ): bool {
    return static::call(in_array(...), [$needle, $this, $strict]);
  }

  /**
   * @see array_search()
   */
  final public function search(
    mixed $needle,
    #[Generator\FixedDefault]
    bool $strict = true,
  ): string|int|false {
    return static::call(array_search(...), [$needle, $this, $strict]);
  }

  /**
   * @see array_fill()
   */
  final public static function fromArrayFill(
    int $start_index,
    int $count,
    mixed $value,
  ): static {
    return new static(static::call(array_fill(...), [$start_index, $count, $value]));
  }

  /**
   * @see array_fill()
   */
  final public function fill(int $start_index, int $count): static {
    return $this->setArray(array_fill($start_index, $count, $this->array));
  }

  /**
   * @see array_fill_keys()
   */
  final public static function fromArrayFillKeys(
    array|ObjectArray $keys,
    mixed $value,
  ): static {
    return new static(static::call(array_fill_keys(...), [$keys, $value]));
  }

  /**
   * @see array_fill_keys()
   */
  final public function fillKeys(mixed $value): static {
    return $this->setArray(static::call(array_fill_keys(...), [$this, $value]));
  }

  /**
   * @see range()
   */
  final public static function fromRange(
    string|int|float $start,
    string|int|float $end,
    int|float $step = 1,
  ): static {
    return new static(range($start, $end, $step));
  }

  /**
   * @see shuffle()
   */
  final public function shuffle(): static {
    shuffle($this->array);
    return $this;
  }

  /**
   * @see array_pop()
   */
  final public static function fromArrayPop(array|ObjectArray &$array): static {
    return new static(static::call(array_pop(...), [&$array]));
  }

  /**
   * @see array_pop()
   */
  final public function pop(): mixed {
    return $this->applyResult(array_pop($this->array));
  }

  /**
   * @see array_shift()
   */
  final public static function fromArrayShift(
    array|ObjectArray &$array,
  ): static {
    return new static(static::call(array_shift(...), [&$array]));
  }

  /**
   * @see array_shift()
   */
  final public function shift(): mixed {
    return $this->applyResult(array_shift($this->array));
  }

  /**
   * @see array_unshift()
   */
  final public function unshift(mixed ...$values): int {
    return static::call(array_unshift(...), [$this, ...$values]);
  }

  /**
   * @see array_splice()
   */
  final public static function fromArraySplice(
    array|ObjectArray &$array,
    int $offset,
    ?int $length = null,
    mixed $replacement = [],
  ): static {
    return new static(static::call(array_splice(...), [&$array, $offset, $length, $replacement]));
  }

  /**
   * @see array_splice()
   */
  final public function splice(
    int $offset,
    ?int $length = null,
    mixed $replacement = [],
  ): static {
    return $this->setArray(static::call(array_splice(...), [$this, $offset, $length, $replacement]));
  }

  /**
   * @see array_slice()
   */
  final public static function fromArraySlice(
    array|ObjectArray $array,
    int $offset,
    ?int $length = null,
    bool $preserve_keys = false,
  ): static {
    return new static(static::call(array_slice(...), [$array, $offset, $length, $preserve_keys]));
  }

  /**
   * @see array_slice()
   */
  final public function slice(
    int $offset,
    ?int $length = null,
    bool $preserve_keys = false,
  ): static {
    return $this->setArray(array_slice($this->array, $offset, $length, $preserve_keys));
  }

  /**
   * @see array_merge()
   */
  final public static function fromArrayMerge(
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_merge(...), $arrays));
  }

  /**
   * @see array_merge()
   */
  final public function merge(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_merge(...), [$this, ...$arrays]));
  }

  /**
   * @see array_merge_recursive()
   */
  final public static function fromArrayMergeRecursive(
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_merge_recursive(...), $arrays));
  }

  /**
   * @see array_merge_recursive()
   */
  final public function mergeRecursive(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_merge_recursive(...), [$this, ...$arrays]));
  }

  /**
   * @see array_replace()
   */
  final public static function fromArrayReplace(
    array|ObjectArray $array,
    array|ObjectArray ...$replacements,
  ): static {
    return new static(static::call(array_replace(...), [$array, ...$replacements]));
  }

  /**
   * @see array_replace()
   */
  final public function replace(array|ObjectArray ...$replacements): static {
    return $this->setArray(static::call(array_replace(...), [$this, ...$replacements]));
  }

  /**
   * @see array_replace_recursive()
   */
  final public static function fromArrayReplaceRecursive(
    array|ObjectArray $array,
    array|ObjectArray ...$replacements,
  ): static {
    return new static(static::call(array_replace_recursive(...), [$array, ...$replacements]));
  }

  /**
   * @see array_replace_recursive()
   */
  final public function replaceRecursive(
    array|ObjectArray ...$replacements,
  ): static {
    return $this->setArray(static::call(array_replace_recursive(...), [$this, ...$replacements]));
  }

  /**
   * @param array|\M22\ObjectArray $array
   * @param mixed $filter_value (optional)
   * @param bool $strict = false (optional)
   *
   * @see array_keys()
   */
  final public static function fromArrayKeys(array|ObjectArray $array): static {
    return new static(static::call(array_keys(...), func_get_args()));
  }

  /**
   * @param mixed $filter_value (optional)
   * @param bool $strict = false (optional)
   *
   * @see array_keys()
   */
  final public function keys(): static {
    return $this->setArray(static::call(array_keys(...), [$this, ...func_get_args()]));
  }

  /**
   * @see array_key_first()
   */
  final public function keyFirst(): string|int|null {
    return array_key_first($this->array);
  }

  /**
   * @see array_key_last()
   */
  final public function keyLast(): string|int|null {
    return array_key_last($this->array);
  }

  /**
   * @see array_values()
   */
  final public static function fromArrayValues(
    array|ObjectArray $array,
  ): static {
    return new static(static::call(array_values(...), [$array]));
  }

  /**
   * @see array_values()
   */
  final public function values(): static {
    return $this->setArray(array_values($this->array));
  }

  /**
   * @see array_count_values()
   */
  final public static function fromArrayCountValues(
    array|ObjectArray $array,
  ): static {
    return new static(static::call(array_count_values(...), [$array]));
  }

  /**
   * @see array_count_values()
   */
  final public function countValues(): static {
    return $this->setArray(array_count_values($this->array));
  }

  /**
   * @see array_column()
   */
  final public static function fromArrayColumn(
    array|ObjectArray $array,
    string|int|null $column_key,
    string|int|null $index_key = null,
  ): static {
    return new static(static::call(array_column(...), [$array, $column_key, $index_key]));
  }

  /**
   * @see array_column()
   */
  final public function column(
    string|int|null $column_key,
    string|int|null $index_key = null,
  ): static {
    return $this->setArray(array_column($this->array, $column_key, $index_key));
  }

  /**
   * @see array_reverse()
   */
  final public static function fromArrayReverse(
    array|ObjectArray $array,
    bool $preserve_keys = false,
  ): static {
    return new static(static::call(array_reverse(...), [$array, $preserve_keys]));
  }

  /**
   * @see array_reverse()
   */
  final public function reverse(bool $preserve_keys = false): static {
    return $this->setArray(array_reverse($this->array, $preserve_keys));
  }

  /**
   * @see array_pad()
   */
  final public static function fromArrayPad(
    array|ObjectArray $array,
    int $length,
    mixed $value,
  ): static {
    return new static(static::call(array_pad(...), [$array, $length, $value]));
  }

  /**
   * @see array_pad()
   */
  final public function pad(int $length, mixed $value): static {
    return $this->setArray(static::call(array_pad(...), [$this, $length, $value]));
  }

  /**
   * @see array_flip()
   */
  final public static function fromArrayFlip(array|ObjectArray $array): static {
    return new static(static::call(array_flip(...), [$array]));
  }

  /**
   * @see array_flip()
   */
  final public function flip(): static {
    return $this->setArray(array_flip($this->array));
  }

  /**
   * @see array_change_key_case()
   */
  final public static function fromArrayChangeKeyCase(
    array|ObjectArray $array,
    int $case = \CASE_LOWER,
  ): static {
    return new static(static::call(array_change_key_case(...), [$array, $case]));
  }

  /**
   * @see array_change_key_case()
   */
  final public function changeKeyCase(int $case = \CASE_LOWER): static {
    return $this->setArray(array_change_key_case($this->array, $case));
  }

  /**
   * @see array_unique()
   */
  final public static function fromArrayUnique(
    array|ObjectArray $array,
    int $flags = \SORT_STRING,
  ): static {
    return new static(static::call(array_unique(...), [$array, $flags]));
  }

  /**
   * @see array_unique()
   */
  final public function unique(int $flags = \SORT_STRING): static {
    return $this->setArray(array_unique($this->array, $flags));
  }

  /**
   * @see array_intersect_key()
   */
  final public static function fromArrayIntersectKey(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_intersect_key(...), [$array, ...$arrays]));
  }

  /**
   * @see array_intersect_key()
   */
  final public function intersectKey(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_intersect_key(...), [$this, ...$arrays]));
  }

  /**
   * @see array_intersect_ukey()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public static function fromArrayIntersectUkey(
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_intersect_ukey(...), [$array, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_intersect_ukey()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public function intersectUkey(
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_intersect_ukey(...), [$this, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_intersect()
   */
  final public static function fromArrayIntersect(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_intersect(...), [$array, ...$arrays]));
  }

  /**
   * @see array_intersect()
   */
  final public function intersect(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_intersect(...), [$this, ...$arrays]));
  }

  /**
   * @see array_uintersect()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public static function fromArrayUintersect(
    callable $value_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_uintersect(...), [$array, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_uintersect()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public function uintersect(
    callable $value_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_uintersect(...), [$this, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_intersect_assoc()
   */
  final public static function fromArrayIntersectAssoc(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_intersect_assoc(...), [$array, ...$arrays]));
  }

  /**
   * @see array_intersect_assoc()
   */
  final public function intersectAssoc(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_intersect_assoc(...), [$this, ...$arrays]));
  }

  /**
   * @see array_uintersect_assoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public static function fromArrayUintersectAssoc(
    callable $value_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_uintersect_assoc(...), [$array, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_uintersect_assoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public function uintersectAssoc(
    callable $value_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_uintersect_assoc(...), [$this, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_intersect_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public static function fromArrayIntersectUassoc(
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_intersect_uassoc(...), [$array, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_intersect_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public function intersectUassoc(
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_intersect_uassoc(...), [$this, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_uintersect_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func', 'key_compare_func')]
  final public static function fromArrayUintersectUassoc(
    callable $value_compare_func,
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_uintersect_uassoc(...), [$array, ...$arrays, $value_compare_func, $key_compare_func]));
  }

  /**
   * @see array_uintersect_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func', 'key_compare_func')]
  final public function uintersectUassoc(
    callable $value_compare_func,
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_uintersect_uassoc(...), [$this, ...$arrays, $value_compare_func, $key_compare_func]));
  }

  /**
   * @see array_diff_key()
   */
  final public static function fromArrayDiffKey(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_diff_key(...), [$array, ...$arrays]));
  }

  /**
   * @see array_diff_key()
   */
  final public function diffKey(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_diff_key(...), [$this, ...$arrays]));
  }

  /**
   * @see array_diff_ukey()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public static function fromArrayDiffUkey(
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_diff_ukey(...), [$array, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_diff_ukey()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public function diffUkey(
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_diff_ukey(...), [$this, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_diff()
   */
  final public static function fromArrayDiff(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_diff(...), [$array, ...$arrays]));
  }

  /**
   * @see array_diff()
   */
  final public function diff(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_diff(...), [$this, ...$arrays]));
  }

  /**
   * @see array_udiff()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public static function fromArrayUdiff(
    callable $value_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_udiff(...), [$array, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_udiff()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public function udiff(
    callable $value_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_udiff(...), [$this, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_diff_assoc()
   */
  final public static function fromArrayDiffAssoc(
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_diff_assoc(...), [$array, ...$arrays]));
  }

  /**
   * @see array_diff_assoc()
   */
  final public function diffAssoc(array|ObjectArray ...$arrays): static {
    return $this->setArray(static::call(array_diff_assoc(...), [$this, ...$arrays]));
  }

  /**
   * @see array_diff_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public static function fromArrayDiffUassoc(
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_diff_uassoc(...), [$array, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_diff_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'key_compare_func')]
  final public function diffUassoc(
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_diff_uassoc(...), [$this, ...$arrays, $key_compare_func]));
  }

  /**
   * @see array_udiff_assoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public static function fromArrayUdiffAssoc(
    callable $value_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_udiff_assoc(...), [$array, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_udiff_assoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func')]
  final public function udiffAssoc(
    callable $value_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_udiff_assoc(...), [$this, ...$arrays, $value_compare_func]));
  }

  /**
   * @see array_udiff_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func', 'key_compare_func')]
  final public static function fromArrayUdiffUassoc(
    callable $value_compare_func,
    callable $key_compare_func,
    array|ObjectArray $array,
    ObjectArray|array ...$arrays,
  ): static {
    return new static(static::call(array_udiff_uassoc(...), [$array, ...$arrays, $value_compare_func, $key_compare_func]));
  }

  /**
   * @see array_udiff_uassoc()
   */
  #[Generator\SplitVariadic('arrays', 'value_compare_func', 'key_compare_func')]
  final public function udiffUassoc(
    callable $value_compare_func,
    callable $key_compare_func,
    ObjectArray|array ...$arrays,
  ): static {
    return $this->setArray(static::call(array_udiff_uassoc(...), [$this, ...$arrays, $value_compare_func, $key_compare_func]));
  }

  /**
   * @see array_rand()
   */
  final public static function fromArrayRand(
    array|ObjectArray $array,
    #[Generator\FixedDefault]
    int $num,
  ): static {
    return new static(static::call(array_rand(...), [$array, $num]));
  }

  /**
   * @see array_rand()
   */
  final public function rand(int $num = 1): static|string|int {
    return $this->applyResult(array_rand($this->array, $num));
  }

  /**
   * @see array_sum()
   */
  final public function sum(): int|float {
    return array_sum($this->array);
  }

  /**
   * @see array_product()
   */
  final public function product(): int|float {
    return array_product($this->array);
  }

  /**
   * @see array_reduce()
   */
  final public static function fromArrayReduce(
    array|ObjectArray $array,
    callable $callback,
    mixed $initial = null,
  ): static {
    return new static(static::call(array_reduce(...), [$array, $callback, $initial]));
  }

  /**
   * @see array_reduce()
   */
  final public function reduce(
    callable $callback,
    mixed $initial = null,
  ): mixed {
    return $this->apply(array_reduce(...), [$this, $callback, $initial]);
  }

  /**
   * @see array_filter()
   */
  final public static function fromArrayFilter(
    array|ObjectArray $array,
    ?callable $callback = null,
    int $mode = 0,
  ): static {
    return new static(static::call(array_filter(...), [$array, $callback, $mode]));
  }

  /**
   * @see array_filter()
   */
  final public function filter(
    ?callable $callback = null,
    int $mode = 0,
  ): static {
    return $this->setArray(array_filter($this->array, $callback, $mode));
  }

  /**
   * @see array_map()
   */
  final public static function fromArrayMap(
    ?callable $callback,
    array|ObjectArray $array,
    array|ObjectArray ...$arrays,
  ): static {
    return new static(static::call(array_map(...), [$callback, $array, ...$arrays]));
  }

  /**
   * @see array_map()
   */
  final public function map(
    ?callable $callback,
    array|ObjectArray ...$arrays,
  ): static {
    return $this->setArray(static::call(array_map(...), [$callback, $this, ...$arrays]));
  }

  /**
   * @param string|int|float|bool|resource|null $key
   *
   * @see array_key_exists()
   */
  final public function keyExists($key): bool {
    return array_key_exists($key, $this->array);
  }

  /**
   * @see array_chunk()
   */
  final public static function fromArrayChunk(
    array|ObjectArray $array,
    int $length,
    bool $preserve_keys = false,
  ): static {
    return new static(static::call(array_chunk(...), [$array, $length, $preserve_keys]));
  }

  /**
   * @see array_chunk()
   */
  final public function chunk(
    int $length,
    bool $preserve_keys = false,
  ): static {
    return $this->setArray(array_chunk($this->array, $length, $preserve_keys));
  }

  /**
   * @see array_combine()
   */
  final public static function fromArrayCombine(
    array|ObjectArray $keys,
    array|ObjectArray $values,
  ): static {
    return new static(static::call(array_combine(...), [$keys, $values]));
  }

  /**
   * @see array_combine()
   */
  final public function combine(array|ObjectArray $values): static {
    return $this->setArray(static::call(array_combine(...), [$this, $values]));
  }

  /**
   * @see array_is_list()
   */
  final public function isList(): bool {
    return array_is_list($this->array);
  }

  /**
   * @see constant()
   */
  final public static function fromConstant(string $name): static {
    return new static(constant($name));
  }

  /**
   * @see getenv()
   */
  #[Generator\FixedArg('name', null)]
  #[Generator\FixedArg('local_only', false)]
  final public static function fromGetenv(): static {
    return new static(getenv());
  }

  /**
   * @see getopt()
   */
  final public static function fromGetopt(
    string $short_options,
    array|ObjectArray $long_options = [],
    ?int &$rest_index = null,
  ): static {
    return new static(static::call(getopt(...), [$short_options, $long_options, &$rest_index]));
  }

  /**
   * @see getopt()
   */
  final public function getopt(
    string $short_options,
    ?int &$rest_index = null,
  ): static|false {
    return $this->applyResult(getopt($short_options, $this->array, $rest_index));
  }

  /**
   * @see get_cfg_var()
   */
  final public static function fromCfgVar(string $option): static {
    return new static(get_cfg_var($option));
  }

  /**
   * @see error_get_last()
   */
  final public static function fromErrorGetLast(): static {
    return new static(error_get_last());
  }

  /**
   * @see register_shutdown_function()
   */
  final public function registerShutdownFunction(
    callable $callback,
    mixed ...$args,
  ): static {
    static::call(register_shutdown_function(...), [$callback, $this, ...$args]);
    return $this;
  }

  /**
   * @see ini_get_all()
   */
  final public static function fromIniGetAll(
    ?string $extension = null,
    bool $details = true,
  ): static {
    return new static(ini_get_all($extension, $details));
  }

  /**
   * @see print_r()
   */
  final public function printR(bool $return = false): string|bool {
    return print_r($this->array, $return);
  }

  /**
   * @see register_tick_function()
   */
  final public function registerTickFunction(
    callable $callback,
    mixed ...$args,
  ): bool {
    return static::call(register_tick_function(...), [$callback, $this, ...$args]);
  }

  /**
   * @see parse_ini_file()
   */
  final public static function fromParseIniFile(
    string $filename,
    bool $process_sections = false,
    int $scanner_mode = \INI_SCANNER_NORMAL,
  ): static {
    return new static(parse_ini_file($filename, $process_sections, $scanner_mode));
  }

  /**
   * @see parse_ini_string()
   */
  final public static function fromParseIniString(
    string $ini_string,
    bool $process_sections = false,
    int $scanner_mode = \INI_SCANNER_NORMAL,
  ): static {
    return new static(parse_ini_string($ini_string, $process_sections, $scanner_mode));
  }

  /**
   * @see sys_getloadavg()
   */
  final public static function fromSysGetloadavg(): static {
    return new static(sys_getloadavg());
  }

  /**
   * @see get_browser()
   */
  #[Generator\FixedArg('return_array', true)]
  final public static function fromBrowser(?string $user_agent = null): static {
    return new static(get_browser($user_agent, true));
  }

  /**
   * @see strptime()
   */
  final public static function fromStrptime(
    string $timestamp,
    string $format,
  ): static {
    return new static(strptime($timestamp, $format));
  }

  /**
   * @see gethostbynamel()
   */
  final public static function fromGethostbynamel(string $hostname): static {
    return new static(gethostbynamel($hostname));
  }

  /**
   * @see dns_get_record()
   */
  final public static function fromDnsGetRecord(
    string $hostname,
    int $type = \DNS_ANY,
    ObjectArray|array|null &$authoritative_name_servers = null,
    ObjectArray|array|null &$additional_records = null,
    bool $raw = false,
  ): static {
    return new static(static::call(dns_get_record(...), [$hostname, $type, &$authoritative_name_servers, &$additional_records, $raw]));
  }

  /**
   * @see net_get_interfaces()
   */
  final public static function fromNetGetInterfaces(): static {
    return new static(net_get_interfaces());
  }

  /**
   * @see hrtime()
   */
  #[Generator\FixedArg('as_number', false)]
  final public static function fromHrtime(): static {
    return new static(hrtime());
  }

  /**
   * @see setrawcookie()
   */
  #[Generator\FixedArg('path', '')]
  #[Generator\FixedArg('domain', '')]
  #[Generator\FixedArg('secure', false)]
  #[Generator\FixedArg('httponly', false)]
  final public function setrawcookie(string $name, string $value = ''): bool {
    return setrawcookie($name, $value, $this->array);
  }

  /**
   * @see setcookie()
   */
  #[Generator\FixedArg('path', '')]
  #[Generator\FixedArg('domain', '')]
  #[Generator\FixedArg('secure', false)]
  #[Generator\FixedArg('httponly', false)]
  final public function setcookie(string $name, string $value = ''): bool {
    return setcookie($name, $value, $this->array);
  }

  /**
   * @see headers_list()
   */
  final public static function fromHeadersList(): static {
    return new static(headers_list());
  }

  /**
   * @see get_html_translation_table()
   */
  final public static function fromHtmlTranslationTable(
    int $table = \HTML_SPECIALCHARS,
    int $flags = 11,
    string $encoding = 'UTF-8',
  ): static {
    return new static(get_html_translation_table($table, $flags, $encoding));
  }

  /**
   * @see explode()
   */
  final public static function fromExplode(
    string $separator,
    string $string,
    int $limit = \PHP_INT_MAX,
  ): static {
    return new static(explode($separator, $string, $limit));
  }

  /**
   * @see implode()
   */
  final public function implode(
    #[Generator\FixedDefault, Generator\FixedTypes]
    string $separator = '',
  ): string {
    return implode($separator, $this->array);
  }

  /**
   * @see join()
   */
  final public function join(
    #[Generator\FixedDefault, Generator\FixedTypes]
    string $separator = '',
  ): string {
    return join($separator, $this->array);
  }

  /**
   * @see pathinfo()
   */
  #[Generator\FixedArg('flags', 15)]
  final public static function fromPathinfo(string $path): static {
    return new static(pathinfo($path));
  }

  /**
   * @see substr_replace()
   */
  final public static function fromSubstrReplace(
    #[Generator\FixedTypes]
    ObjectArray|array $string,
    array|string|ObjectArray $replace,
    array|int|ObjectArray $offset,
    array|int|null|ObjectArray $length = null,
  ): static {
    return new static(static::call(substr_replace(...), [$string, $replace, $offset, $length]));
  }

  /**
   * @see substr_replace()
   */
  #[Generator\FixedReturnTypes('array')]
  final public function substrReplace(
    array|string|ObjectArray $replace,
    array|int|ObjectArray $offset,
    array|int|null|ObjectArray $length = null,
  ): static {
    return $this->setArray(static::call(substr_replace(...), [$this, $replace, $offset, $length]));
  }

  /**
   * @see strtr()
   */
  #[Generator\FixedArg('to', null)]
  final public function strtr(string $string): string {
    return strtr($string, $this->array);
  }

  /**
   * @see str_replace()
   */
  final public static function fromStrReplace(
    array|string|ObjectArray $search,
    array|string|ObjectArray $replace,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    ?int &$count = null,
  ): static {
    return new static(static::call(str_replace(...), [$search, $replace, $subject, &$count]));
  }

  /**
   * @see str_replace()
   */
  #[Generator\FixedReturnTypes('array')]
  final public function strReplace(
    array|string|ObjectArray $search,
    array|string|ObjectArray $replace,
    ?int &$count = null,
  ): static {
    return $this->setArray(static::call(str_replace(...), [$search, $replace, $this, &$count]));
  }

  /**
   * @see str_ireplace()
   */
  final public static function fromStrIreplace(
    array|string|ObjectArray $search,
    array|string|ObjectArray $replace,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    ?int &$count = null,
  ): static {
    return new static(static::call(str_ireplace(...), [$search, $replace, $subject, &$count]));
  }

  /**
   * @see str_ireplace()
   */
  #[Generator\FixedReturnTypes('array')]
  final public function strIreplace(
    array|string|ObjectArray $search,
    array|string|ObjectArray $replace,
    ?int &$count = null,
  ): static {
    return $this->setArray(static::call(str_ireplace(...), [$search, $replace, $this, &$count]));
  }

  /**
   * @see strip_tags()
   */
  final public function stripTags(string $string): string {
    return strip_tags($string, $this->array);
  }

  /**
   * @see str_getcsv()
   */
  final public static function fromStrGetcsv(
    string $string,
    string $separator = ',',
    string $enclosure = '"',
    string $escape = '\\',
  ): static {
    return new static(str_getcsv($string, $separator, $enclosure, $escape));
  }

  /**
   * @see count_chars()
   */
  final public static function fromCountChars(
    string $string,
    int $mode = 0,
  ): static {
    return new static(count_chars($string, $mode));
  }

  /**
   * @see localeconv()
   */
  final public static function fromLocaleconv(): static {
    return new static(localeconv());
  }

  /**
   * @see sscanf()
   */
  #[Generator\FixedArg('vars', [])]
  final public static function fromSscanf(
    string $string,
    string $format,
  ): static {
    return new static(sscanf($string, $format));
  }

  /**
   * @see str_word_count()
   */
  final public static function fromStrWordCount(
    string $string,
    #[Generator\FixedDefault]
    int $format,
    ?string $characters = null,
  ): static {
    return new static(str_word_count($string, $format, $characters));
  }

  /**
   * @see str_split()
   */
  final public static function fromStrSplit(
    string $string,
    int $length = 1,
  ): static {
    return new static(str_split($string, $length));
  }

  /**
   * @param string $directory
   * @param ?resource $context = null (optional)
   *
   * @see dir()
   */
  final public static function fromDir(
    string $directory,
    $context = null,
  ): static {
    return new static(dir($directory, $context));
  }

  /**
   * @param string $directory
   * @param int $sorting_order = \SCANDIR_SORT_ASCENDING (optional)
   * @param ?resource $context = null (optional)
   *
   * @see scandir()
   */
  final public static function fromScandir(
    string $directory,
    int $sorting_order = \SCANDIR_SORT_ASCENDING,
    $context = null,
  ): static {
    return new static(scandir($directory, $sorting_order, $context));
  }

  /**
   * @see glob()
   */
  final public static function fromGlob(
    string $pattern,
    int $flags = 0,
  ): static {
    return new static(glob($pattern, $flags));
  }

  /**
   * @see get_meta_tags()
   */
  final public static function fromMetaTags(
    string $filename,
    bool $use_include_path = false,
  ): static {
    return new static(get_meta_tags($filename, $use_include_path));
  }

  /**
   * @param resource $stream
   * @param string $format
   *
   * @see fscanf()
   */
  #[Generator\FixedArg('vars', [])]
  final public static function fromFscanf($stream, string $format): static {
    return new static(fscanf($stream, $format));
  }

  /**
   * @param resource $stream
   *
   * @see fstat()
   */
  final public static function fromFstat($stream): static {
    return new static(fstat($stream));
  }

  /**
   * @param string $filename
   * @param int $flags = 0 (optional)
   * @param ?resource $context = null (optional)
   *
   * @see file()
   */
  final public static function fromFile(
    string $filename,
    int $flags = 0,
    $context = null,
  ): static {
    return new static(file($filename, $flags, $context));
  }

  /**
   * @param string $filename
   * @param int $flags = 0 (optional)
   * @param ?resource $context = null (optional)
   *
   * @see file_put_contents()
   */
  final public function filePutContents(
    string $filename,
    int $flags = 0,
    $context = null,
  ): int|false {
    return file_put_contents($filename, $this->array, $flags, $context);
  }

  /**
   * @param resource $stream
   * @param string $separator = ',' (optional)
   * @param string $enclosure = '"' (optional)
   * @param string $escape = '\\' (optional)
   * @param string $eol = "\n" (optional)
   *
   * @see fputcsv()
   */
  final public function fputcsv(
    $stream,
    string $separator = ',',
    string $enclosure = '"',
    string $escape = '\\',
    string $eol = "\n",
  ): int|false {
    return fputcsv($stream, $this->array, $separator, $enclosure, $escape, $eol);
  }

  /**
   * @param resource $stream
   * @param ?int $length = null (optional)
   * @param string $separator = ',' (optional)
   * @param string $enclosure = '"' (optional)
   * @param string $escape = '\\' (optional)
   *
   * @see fgetcsv()
   */
  final public static function fromFgetcsv(
    $stream,
    ?int $length = null,
    string $separator = ',',
    string $enclosure = '"',
    string $escape = '\\',
  ): static {
    return new static(fgetcsv($stream, $length, $separator, $enclosure, $escape));
  }

  /**
   * @see stat()
   */
  final public static function fromStat(string $filename): static {
    return new static(stat($filename));
  }

  /**
   * @see lstat()
   */
  final public static function fromLstat(string $filename): static {
    return new static(lstat($filename));
  }

  /**
   * @see realpath_cache_get()
   */
  final public static function fromRealpathCacheGet(): static {
    return new static(realpath_cache_get());
  }

  /**
   * @see vprintf()
   */
  final public function vprintf(string $format): int {
    return vprintf($format, $this->array);
  }

  /**
   * @see vsprintf()
   */
  final public function vsprintf(string $format): string {
    return vsprintf($format, $this->array);
  }

  /**
   * @param resource $stream
   * @param string $format
   *
   * @see vfprintf()
   */
  final public function vfprintf($stream, string $format): int {
    return vfprintf($stream, $format, $this->array);
  }

  /**
   * @see http_build_query()
   */
  final public function httpBuildQuery(
    string $numeric_prefix = '',
    ?string $arg_separator = null,
    int $encoding_type = \PHP_QUERY_RFC1738,
  ): string {
    return http_build_query($this->array, $numeric_prefix, $arg_separator, $encoding_type);
  }

  /**
   * @see getimagesize()
   */
  final public static function fromGetimagesize(
    string $filename,
    ObjectArray|array|null &$image_info = null,
  ): static {
    return new static(static::call(getimagesize(...), [$filename, &$image_info]));
  }

  /**
   * @see getimagesizefromstring()
   */
  final public static function fromGetimagesizefromstring(
    string $string,
    ObjectArray|array|null &$image_info = null,
  ): static {
    return new static(static::call(getimagesizefromstring(...), [$string, &$image_info]));
  }

  /**
   * @see iptcparse()
   */
  final public static function fromIptcparse(string $iptc_block): static {
    return new static(iptcparse($iptc_block));
  }

  /**
   * @see mail()
   */
  final public function mail(
    string $to,
    string $subject,
    string $message,
    string $additional_params = '',
  ): bool {
    return mail($to, $subject, $message, $this->array, $additional_params);
  }

  /**
   * @see gettimeofday()
   */
  #[Generator\FixedArg('as_float', false)]
  final public static function fromGettimeofday(): static {
    return new static(gettimeofday());
  }

  /**
   * @see getrusage()
   */
  final public static function fromGetrusage(int $mode = 0): static {
    return new static(getrusage($mode));
  }

  /**
   * @see unpack()
   */
  final public static function fromUnpack(
    string $format,
    string $string,
    int $offset = 0,
  ): static {
    return new static(unpack($format, $string, $offset));
  }

  /**
   * @see password_get_info()
   */
  final public static function fromPasswordGetInfo(string $hash): static {
    return new static(password_get_info($hash));
  }

  /**
   * @see password_hash()
   */
  final public function passwordHash(
    #[\SensitiveParameter]
    string $password,
    string|int|null $algo,
  ): string {
    return password_hash($password, $algo, $this->array);
  }

  /**
   * @see password_needs_rehash()
   */
  final public function passwordNeedsRehash(
    string $hash,
    string|int|null $algo,
  ): bool {
    return password_needs_rehash($hash, $algo, $this->array);
  }

  /**
   * @see password_algos()
   */
  final public static function fromPasswordAlgos(): static {
    return new static(password_algos());
  }

  /**
   * @return resource|false
   *
   * @see proc_open()
   */
  final public function procOpen(
    array|ObjectArray $descriptor_spec,
    ObjectArray|array &$pipes,
    ?string $cwd = null,
    array|null|ObjectArray $env_vars = null,
    array|null|ObjectArray $options = null,
  ) {
    return static::call(proc_open(...), [$this, $descriptor_spec, &$pipes, $cwd, $env_vars, $options]);
  }

  /**
   * @param resource $process
   *
   * @see proc_get_status()
   */
  final public static function fromProcGetStatus($process): static {
    return new static(proc_get_status($process));
  }

  /**
   * @see stream_select()
   */
  final public function streamSelect(
    array|null|ObjectArray &$write,
    array|null|ObjectArray &$except,
    ?int $seconds,
    ?int $microseconds = null,
  ): int|false {
    return static::call(stream_select(...), [$this, &$write, &$except, $seconds, $microseconds]);
  }

  /**
   * @return resource
   *
   * @see stream_context_create()
   */
  final public function streamContextCreate(
    array|null|ObjectArray $params = null,
  ) {
    return static::call(stream_context_create(...), [$this, $params]);
  }

  /**
   * @param resource $context
   *
   * @see stream_context_set_params()
   */
  final public function streamContextSetParams($context): bool {
    return stream_context_set_params($context, $this->array);
  }

  /**
   * @param resource $context
   *
   * @see stream_context_get_params()
   */
  final public static function fromStreamContextGetParams($context): static {
    return new static(stream_context_get_params($context));
  }

  /**
   * @param resource $context
   *
   * @see stream_context_set_options()
   */
  final public function streamContextSetOptions($context): bool {
    return stream_context_set_options($context, $this->array);
  }

  /**
   * @param resource $stream_or_context
   *
   * @see stream_context_get_options()
   */
  final public static function fromStreamContextGetOptions(
    $stream_or_context,
  ): static {
    return new static(stream_context_get_options($stream_or_context));
  }

  /**
   * @return resource
   *
   * @see stream_context_get_default()
   */
  final public function streamContextGetDefault() {
    return stream_context_get_default($this->array);
  }

  /**
   * @return resource
   *
   * @see stream_context_set_default()
   */
  final public function streamContextSetDefault() {
    return stream_context_set_default($this->array);
  }

  /**
   * @param resource $stream
   * @param string $filter_name
   * @param int $mode = 0 (optional)
   *
   * @return resource|false
   *
   * @see stream_filter_prepend()
   */
  final public function streamFilterPrepend(
    $stream,
    string $filter_name,
    int $mode = 0,
  ) {
    return stream_filter_prepend($stream, $filter_name, $mode, $this->array);
  }

  /**
   * @param resource $stream
   * @param string $filter_name
   * @param int $mode = 0 (optional)
   *
   * @return resource|false
   *
   * @see stream_filter_append()
   */
  final public function streamFilterAppend(
    $stream,
    string $filter_name,
    int $mode = 0,
  ) {
    return stream_filter_append($stream, $filter_name, $mode, $this->array);
  }

  /**
   * @see stream_socket_pair()
   */
  final public static function fromStreamSocketPair(
    int $domain,
    int $type,
    int $protocol,
  ): static {
    return new static(stream_socket_pair($domain, $type, $protocol));
  }

  /**
   * @param resource $stream
   *
   * @see stream_get_meta_data()
   */
  final public static function fromStreamGetMetaData($stream): static {
    return new static(stream_get_meta_data($stream));
  }

  /**
   * @param resource $stream
   *
   * @see socket_get_status()
   */
  final public static function fromSocketGetStatus($stream): static {
    return new static(socket_get_status($stream));
  }

  /**
   * @see stream_get_wrappers()
   */
  final public static function fromStreamGetWrappers(): static {
    return new static(stream_get_wrappers());
  }

  /**
   * @see stream_get_transports()
   */
  final public static function fromStreamGetTransports(): static {
    return new static(stream_get_transports());
  }

  /**
   * @see parse_url()
   */
  #[Generator\FixedArg('component', -1)]
  final public static function fromParseUrl(string $url): static {
    return new static(parse_url($url));
  }

  /**
   * @param string $url
   * @param bool $associative = false (optional)
   * @param ?resource $context = null (optional)
   *
   * @see get_headers()
   */
  final public static function fromHeaders(
    string $url,
    bool $associative = false,
    $context = null,
  ): static {
    return new static(get_headers($url, $associative, $context));
  }

  /**
   * @param resource $brigade
   *
   * @see stream_bucket_make_writeable()
   */
  final public static function fromStreamBucketMakeWriteable($brigade): static {
    return new static(stream_bucket_make_writeable($brigade));
  }

  /**
   * @param resource $stream
   * @param string $buffer
   *
   * @see stream_bucket_new()
   */
  final public static function fromStreamBucketNew(
    $stream,
    string $buffer,
  ): static {
    return new static(stream_bucket_new($stream, $buffer));
  }

  /**
   * @see stream_get_filters()
   */
  final public static function fromStreamGetFilters(): static {
    return new static(stream_get_filters());
  }

  /**
   * @see var_dump()
   */
  final public function varDump(mixed ...$values): static {
    static::call(var_dump(...), [$this, ...$values]);
    return $this;
  }

  /**
   * @see var_export()
   */
  final public function varExport(bool $return = false): ?string {
    return var_export($this->array, $return);
  }

  /**
   * @see debug_zval_dump()
   */
  final public function debugZvalDump(mixed ...$values): static {
    static::call(debug_zval_dump(...), [$this, ...$values]);
    return $this;
  }

  /**
   * @see serialize()
   */
  final public function serialize(): string {
    return serialize($this->array);
  }

  /**
   * @see unserialize()
   */
  final public static function fromUnserialize(
    string $data,
    array|ObjectArray $options = [],
  ): static {
    return new static(static::call(unserialize(...), [$data, $options]));
  }

  /**
   * @see unserialize()
   */
  final public function unserialize(string $data): mixed {
    return $this->applyResult(unserialize($data, $this->array));
  }

  /**
   * @see class_implements()
   */
  final public static function fromClassImplements(
    object|string $object_or_class,
    bool $autoload = true,
  ): static {
    return new static(class_implements($object_or_class, $autoload));
  }

  /**
   * @see class_parents()
   */
  final public static function fromClassParents(
    object|string $object_or_class,
    bool $autoload = true,
  ): static {
    return new static(class_parents($object_or_class, $autoload));
  }

  /**
   * @see class_uses()
   */
  final public static function fromClassUses(
    object|string $object_or_class,
    bool $autoload = true,
  ): static {
    return new static(class_uses($object_or_class, $autoload));
  }

  /**
   * @see spl_autoload_functions()
   */
  final public static function fromSplAutoloadFunctions(): static {
    return new static(spl_autoload_functions());
  }

  /**
   * @see spl_classes()
   */
  final public static function fromSplClasses(): static {
    return new static(spl_classes());
  }

  /**
   * @see localtime()
   */
  final public static function fromLocaltime(
    ?int $timestamp = null,
    bool $associative = false,
  ): static {
    return new static(localtime($timestamp, $associative));
  }

  /**
   * @see getdate()
   */
  final public static function fromGetdate(?int $timestamp = null): static {
    return new static(getdate($timestamp));
  }

  /**
   * @see date_parse()
   */
  final public static function fromDateParse(string $datetime): static {
    return new static(date_parse($datetime));
  }

  /**
   * @see date_parse_from_format()
   */
  final public static function fromDateParseFromFormat(
    string $format,
    string $datetime,
  ): static {
    return new static(date_parse_from_format($format, $datetime));
  }

  /**
   * @see date_get_last_errors()
   */
  final public static function fromDateGetLastErrors(): static {
    return new static(date_get_last_errors());
  }

  /**
   * @see timezone_transitions_get()
   */
  final public static function fromTimezoneTransitionsGet(
    \DateTimeZone $object,
    int $timestampBegin = \PHP_INT_MIN,
    int $timestampEnd = \PHP_INT_MAX,
  ): static {
    return new static(timezone_transitions_get($object, $timestampBegin, $timestampEnd));
  }

  /**
   * @see timezone_location_get()
   */
  final public static function fromTimezoneLocationGet(
    \DateTimeZone $object,
  ): static {
    return new static(timezone_location_get($object));
  }

  /**
   * @see timezone_identifiers_list()
   */
  final public static function fromTimezoneIdentifiersList(
    int $timezoneGroup = \DateTimeZone::ALL,
    ?string $countryCode = null,
  ): static {
    return new static(timezone_identifiers_list($timezoneGroup, $countryCode));
  }

  /**
   * @see timezone_abbreviations_list()
   */
  final public static function fromTimezoneAbbreviationsList(): static {
    return new static(timezone_abbreviations_list());
  }

  /**
   * @see date_sun_info()
   */
  final public static function fromDateSunInfo(
    int $timestamp,
    float $latitude,
    float $longitude,
  ): static {
    return new static(date_sun_info($timestamp, $latitude, $longitude));
  }

  /**
   * @see hash()
   */
  final public function hash(
    string $algo,
    string $data,
    bool $binary = false,
  ): string {
    return hash($algo, $data, $binary, $this->array);
  }

  /**
   * @see hash_file()
   */
  final public function hashFile(
    string $algo,
    string $filename,
    bool $binary = false,
  ): string|false {
    return hash_file($algo, $filename, $binary, $this->array);
  }

  /**
   * @see hash_init()
   */
  final public function hashInit(
    string $algo,
    int $flags = 0,
    #[\SensitiveParameter]
    string $key = '',
  ): \HashContext {
    return hash_init($algo, $flags, $key, $this->array);
  }

  /**
   * @see hash_algos()
   */
  final public static function fromHashAlgos(): static {
    return new static(hash_algos());
  }

  /**
   * @see hash_hmac_algos()
   */
  final public static function fromHashHmacAlgos(): static {
    return new static(hash_hmac_algos());
  }

  /**
   * @see hash_pbkdf2()
   */
  final public function hashPbkdf2(
    string $algo,
    #[\SensitiveParameter]
    string $password,
    string $salt,
    int $iterations,
    int $length = 0,
    bool $binary = false,
  ): string {
    return hash_pbkdf2($algo, $password, $salt, $iterations, $length, $binary, $this->array);
  }

  /**
   * @see json_decode()
   */
  final public static function fromJsonDecode(
    string $json,
    ?bool $associative = null,
    int $depth = 512,
    int $flags = 0,
  ): static {
    return new static(json_decode($json, $associative, $depth, $flags));
  }

  /**
   * @see preg_replace()
   */
  final public static function fromPregReplace(
    array|string|ObjectArray $pattern,
    array|string|ObjectArray $replacement,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    int $limit = -1,
    ?int &$count = null,
  ): static {
    return new static(static::call(preg_replace(...), [$pattern, $replacement, $subject, $limit, &$count]));
  }

  /**
   * @see preg_replace()
   */
  #[Generator\FixedReturnTypes('?array')]
  final public function pregReplace(
    array|string|ObjectArray $pattern,
    array|string|ObjectArray $replacement,
    int $limit = -1,
    ?int &$count = null,
  ): ?static {
    return $this->apply(preg_replace(...), [$pattern, $replacement, $this, $limit, &$count]);
  }

  /**
   * @see preg_filter()
   */
  final public static function fromPregFilter(
    array|string|ObjectArray $pattern,
    array|string|ObjectArray $replacement,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    int $limit = -1,
    ?int &$count = null,
  ): static {
    return new static(static::call(preg_filter(...), [$pattern, $replacement, $subject, $limit, &$count]));
  }

  /**
   * @see preg_filter()
   */
  #[Generator\FixedReturnTypes('array')]
  final public function pregFilter(
    array|string|ObjectArray $pattern,
    array|string|ObjectArray $replacement,
    int $limit = -1,
    ?int &$count = null,
  ): static {
    return $this->setArray(static::call(preg_filter(...), [$pattern, $replacement, $this, $limit, &$count]));
  }

  /**
   * @see preg_replace_callback()
   */
  final public static function fromPregReplaceCallback(
    array|string|ObjectArray $pattern,
    callable $callback,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    int $limit = -1,
    ?int &$count = null,
    int $flags = 0,
  ): static {
    return new static(static::call(preg_replace_callback(...), [$pattern, $callback, $subject, $limit, &$count, $flags]));
  }

  /**
   * @see preg_replace_callback()
   */
  #[Generator\FixedReturnTypes('?array')]
  final public function pregReplaceCallback(
    array|string|ObjectArray $pattern,
    callable $callback,
    int $limit = -1,
    ?int &$count = null,
    int $flags = 0,
  ): ?static {
    return $this->apply(preg_replace_callback(...), [$pattern, $callback, $this, $limit, &$count, $flags]);
  }

  /**
   * @see preg_replace_callback_array()
   */
  final public static function fromPregReplaceCallbackArray(
    array|ObjectArray $pattern,
    #[Generator\FixedTypes]
    ObjectArray|array $subject,
    int $limit = -1,
    ?int &$count = null,
    int $flags = 0,
  ): static {
    return new static(static::call(preg_replace_callback_array(...), [$pattern, $subject, $limit, &$count, $flags]));
  }

  /**
   * @see preg_replace_callback_array()
   */
  #[Generator\FixedReturnTypes('?array')]
  final public function pregReplaceCallbackArray(
    array|ObjectArray $pattern,
    int $limit = -1,
    ?int &$count = null,
    int $flags = 0,
  ): ?static {
    return $this->apply(preg_replace_callback_array(...), [$pattern, $this, $limit, &$count, $flags]);
  }

  /**
   * @see preg_split()
   */
  final public static function fromPregSplit(
    string $pattern,
    string $subject,
    int $limit = -1,
    int $flags = 0,
  ): static {
    return new static(preg_split($pattern, $subject, $limit, $flags));
  }

  /**
   * @see preg_grep()
   */
  final public static function fromPregGrep(
    string $pattern,
    array|ObjectArray $array,
    int $flags = 0,
  ): static {
    return new static(static::call(preg_grep(...), [$pattern, $array, $flags]));
  }

  /**
   * @see preg_grep()
   */
  final public function pregGrep(
    string $pattern,
    int $flags = 0,
  ): static|false {
    return $this->applyResult(preg_grep($pattern, $this->array, $flags));
  }

  /**
   * @see opcache_get_status()
   */
  final public static function fromOpcacheGetStatus(
    bool $include_scripts = true,
  ): static {
    return new static(opcache_get_status($include_scripts));
  }

  /**
   * @see opcache_get_configuration()
   */
  final public static function fromOpcacheGetConfiguration(): static {
    return new static(opcache_get_configuration());
  }
}
