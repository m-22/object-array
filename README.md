# ObjectArray

**Allows arrays to work as objects.**

Use methods chaining instead of nested functions.

```php
use M22\ObjectArray as A;

$a = new A(['a', 'b', 'c']);

$a->diffAssoc($array1)
  ->plus($array2)
  ->sort()
  ->map($callback1)
  ->filter($callback2)
  ->keys()
  // ...

$count_letters = A::fromRange('a', 'z')->fillKeys(0);
$count_letters->t++;

$directories = A::fromExplode('/', $path);
```

Best suited for use as a [Composer](https://getcomposer.org) library.

## Requirements

* PHP &ge; 8.3
* [Composer](https://getcomposer.org)

## Installation

To add this library to your Composer project:
```
composer require m22/object-array
```

## Usage

There are multiple ways to instantiate a new ObjectArray:

```php
$a = new ObjectArray($array_or_object);
$b = ObjectArray::from($array_or_object);
$c = ObjectArray::fromFunction($with_arguments_or_without);
```

Where `fromFunction()` is not a real method, replace the `Function` part in it with any existing function, which normally returns an array or an object, with corresponding arguments.
For example, `fromStrSplit($string, $length)`.
That function's result is then supplied to the constructor internally.
Method optionally accepts other ObjectArray instances as arguments instead of arrays.

In case of instantiating from an array, its copy is stored in the new ObjectArray.
And in case of instantiating from an object, there is a special handling.

- If it's another ObjectArray instance or an \ArrayObject instance, a copy of its internal array is stored in the new ObjectArray.
- If it's a \Traversable instance, a copy of its iterator is stored.
- Otherwise, an associative array of defined accessible non-static object properties is stored.

After that use any function, which accepts an array argument, as an ObjectArray method without supplying an array argument.
For example:

- `$object_array->filter($callback)` is analog of `array_filter($array, $callback)`,
- `$object_array->diffKey($array2)` of `array_diff_key($array, $array2)`,
- `$object_array->reset()` of `reset($array)`, etc.

If the function returns an array, the ObjectArray instance replaces its internal array with that function's result, and returns itself for chaining.
Otherwise, it returns the function's result.
Methods optionally accept other ObjectArray instances as arguments instead of arrays: `$object_array->diffKey($object_array2)`.

An ObjectArray's internal array is stored in the public `array` property (`$object_array->array`), so it can be accessed or overwritten anytime.

There are other cases when an ObjectArray instance behaves the same as an array from which it's constructed:

- Accessing/creating/modifying array elements with square bracket syntax (some of these operations can also be done using object->property syntax)

```php
$a = new ObjectArray(['a', $b, 'C' => $c]);

$a[1] === $b; // True.
$a['C'] === $c; // True.

$a[] = 'd';

$a['E'] = 1;
$a['E']++;

isset($a[0]);
unset($a['C']);
```

- Array destructuring

```php
[$a, $b, $c] = new ObjectArray([1, 2, 3]);
['a' => $a, 'b' => $b] = new ObjectArray(['a' => 1, 'b' => 2])
```

- Array unpacking

```php
$array = [...ObjectArray::from(['a', 'b', 'c'])];
```

- Traversing

```php
foreach (ObjectArray::from(['a' => 1, 'b' => 2]) as $key => $value)
```

- JSON encoding

```php
json_encode(new ObjectArray($array)) === json_encode($array); // True.
```

## Development

When a new major (or minor) PHP version comes out, the ObjectArray methods need to be regenerated.

1. Update PHP versions throughout the project (composer.*, README, etc.)
2. In the `MethodsGenerator` class review/update the `CORE_MODULES` list.
3. Run `composer generate`.
4. Create new git branch, commit changes there, add tag.
